FROM ubuntu:18.04
MAINTAINER Lukas Zimmer <lukas.zimmer@risqitlab.com>

LABEL description="Docker C++ gcc build environment for RISQ"

SHELL ["/bin/bash", "-c"]

# No interaction possible with the system during installation of the libraries
ARG DEBIAN_FRONTEND=noninteractive

# How many cores to use for make
ENV NUM_CORES 4

# Installed versions of self built libraries
ENV OPENMPI_VERSION 4.0.3
ENV ANACONDA_VERSION 2019.07
ENV CMAKE_VERSION 3.15.2
ENV LAPACK_VERSION 3.8.0
ENV ARMADILLO_VERSION 9.700.2
ENV TMUX_VERSION 2.9a
ENV NLOPT_VERSION 2.6.1
ENV FLEX_VERSION 2.6.4
ENV BISON_VERSION 3.4.1
ENV LIBTOOL_VERSION 2.4.6
ENV GLIB_VERSION 2.61.2
ENV LIBFFI_VERSION 3.2.1
ENV UTILLINUX_VERSION 2.34
ENV PCRE_VERSION 8.43
ENV GSL_VERSION 2.6
ENV BOOST_VERSION 1_71_0
ENV MLPACK_VERSION 3.1.1
ENV YAML_VERSION 0.6.2

# Update base image
RUN apt-get update -qq
RUN apt-get install -y \
  software-properties-common lsb-release
RUN apt-get dist-upgrade -qq

# Install common unix/build tools
RUN apt-get install -y \
  clang \
  automake \
  autoconf \
  autoconf-archive \
  libtool \
  libevent-dev \
  libdouble-conversion-dev \
  libgflags-dev \
  liblz4-dev \
  liblzma-dev \
  libsnappy-dev \
  make \
  zlib1g-dev \
  binutils-dev \
  libjemalloc-dev \
  libssl-dev \
  libiberty-dev \
  htop \
  wget \
  lbzip2 \
  bzip2 \
  libbz2-dev \
  git \
  zip \
  txt2man \
  rsync \
  tar \
  ssh \
  gdb

# Directories for self built libs
RUN mkdir /libs
RUN mkdir /libs/tmp
WORKDIR /libs/tmp

# Environment variables for self built libraries
ENV LD_LIBRARY_PATH "/usr/local/lib64:/usr/local/lib:/usr/lib/x86_64-linux-gnu:/usr/lib:/usr/lib64"
ENV LIBRARY_PATH "/usr/local/lib64:/usr/local/lib:/usr/lib/x86_64-linux-gnu:/usr/lib:/usr/lib64"
ENV CPATH "/usr/local/include:/usr/include/x86_64-linux-gnu:/usr/include"
ENV PATH "/usr/local/bin:/usr/bin:${PATH}"

# pkg-config
RUN apt-get install -y apt-utils
RUN git clone https://gitlab.freedesktop.org/pkg-config/pkg-config
RUN cd pkg-config && ./autogen.sh --with-internal-glib && make -j ${NUM_CORES} && make install

# GCC 9
RUN add-apt-repository ppa:ubuntu-toolchain-r/test -y
RUN apt-get update -y
RUN apt-get install -y gcc-9 g++-9 gfortran-9 libgfortran-9-dev
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 \
  --slave /usr/bin/g++ g++ /usr/bin/g++-9 \
  --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-9
RUN update-alternatives --config gcc

ENV CC gcc
ENV CXX g++
ENV OMPI_CC gcc
ENV OMPI_CXX g++
ENV OMPI_FC gfortran
ENV OMPI_F77 gfortran
ENV OMPI_F90 gfortran
ENV F77 gfortran
ENV F90 gfortran
ENV FC gfortran

# Python 3.7
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update -y
RUN apt-get install -y python3.7-dev libpython3.7-dev

# Intel compiler
#ENV INTEL_LICENSE_FILE=28518@scclic1.scc.kit.edu
#RUN wget ftp://ftp.scc.kit.edu/pub/campus/INTEL/parallel_studio_xe_2019_update4_cluster_edition.tgz
#RUN tar -xzf parallel_studio_xe_2019_update4_cluster_edition.tgz
#RUN cd parallel_studio_xe_2019_update4_cluster_edition && \
#  sed -i "/ACCEPT_EULA=/c\ACCEPT_EULA=accept" silent.cfg && \
#  sed -i "/COMPONENTS=/c\COMPONENTS=ALL" silent.cfg && \
#  sed -i "/ARCH_SELECTED=/c\ARCH_SELECTED=INTEL64" silent.cfg
#RUN apt-get install -y cpio
#RUN cd parallel_studio_xe_2019_update4_cluster_edition && ./install.sh --silent=silent.cfg
#COPY intel.sh /etc/profile.d/intel.sh
#ENV BASH_ENV=/etc/profile.d/intel.sh
#ENV CC icc
#ENV CXX icpc
#ENV MPICC icc
#ENV MPICXX icpc
#ENV MPIFC ifort
#ENV MPIF77 ifort
#ENV MPIF90 ifort
#ENV F77 ifort
#ENV F90 ifort
#ENV FC ifort

# CMake
RUN wget https://cmake.org/files/v$(echo ${CMAKE_VERSION} | cut -d. -f1-2)/cmake-${CMAKE_VERSION}.tar.gz
RUN tar -xzf cmake-${CMAKE_VERSION}.tar.gz
RUN cd cmake-${CMAKE_VERSION} && ./bootstrap && make -j ${NUM_CORES} && make install

# OpenMPI
RUN wget https://download.open-mpi.org/release/open-mpi/v$(echo ${OPENMPI_VERSION} | cut -d. -f1-2)/openmpi-${OPENMPI_VERSION}.tar.gz
RUN tar -xzf openmpi-${OPENMPI_VERSION}.tar.gz
RUN cd openmpi-${OPENMPI_VERSION} && ./configure && make -j ${NUM_CORES} && make install

## TBB
## build older version 2019 U2 because of bug on ForHLR1
## see https://software.intel.com/en-us/forums/intel-threading-building-blocks/topic/808343
#RUN git clone https://github.com/wjakob/tbb.git && cd tbb && git checkout b066def
#RUN cd tbb/build && cmake .. && make -j ${NUM_CORES} && make install
#RUN rm -rf /opt/intel/tbb
#ENV TBBROOT /usr/local

# LAPACK (incl BLAS) & Armadillo
RUN wget http://www.netlib.org/lapack/lapack-${LAPACK_VERSION}.tar.gz
RUN tar -xzf lapack-${LAPACK_VERSION}.tar.gz
RUN cd lapack-${LAPACK_VERSION} && mkdir lapackbuild_shared && cd lapackbuild_shared && \
 cmake -DBUILD_SHARED_LIBS=ON .. && make -j ${NUM_CORES} && make install
RUN wget http://sourceforge.net/projects/arma/files/armadillo-${ARMADILLO_VERSION}.tar.xz
RUN tar -xJf armadillo-${ARMADILLO_VERSION}.tar.xz
RUN cd armadillo-${ARMADILLO_VERSION} && mkdir armadillobuild_shared && cd armadillobuild_shared && \
  cmake .. && make -j ${NUM_CORES} && make install
  
# tmux
RUN apt-get install -y libncurses5-dev
RUN wget https://github.com/tmux/tmux/releases/download/${TMUX_VERSION}/tmux-${TMUX_VERSION}.tar.gz
RUN tar -xzf tmux-${TMUX_VERSION}.tar.gz
RUN cd tmux-${TMUX_VERSION} && ./configure && make -j ${NUM_CORES} && make install

# NLopt
RUN wget https://github.com/stevengj/nlopt/archive/v${NLOPT_VERSION}.zip
RUN unzip v${NLOPT_VERSION}.zip
RUN cd nlopt-${NLOPT_VERSION} && mkdir build && cd build && cmake .. && make -j ${NUM_CORES} && make install

# GSL
RUN wget ftp://ftp.gnu.org/gnu/gsl/gsl-${GSL_VERSION}.tar.gz
RUN tar -xzf gsl-${GSL_VERSION}.tar.gz
RUN cd gsl-${GSL_VERSION} && ./configure && make -j ${NUM_CORES} && make install

# Google Test & Google Mock
RUN git clone https://github.com/google/googletest.git 
RUN cd googletest && mkdir googletestbuild && cd googletestbuild \
  && cmake .. && make -j ${NUM_CORES} && make install

# Boost
RUN wget https://dl.bintray.com/boostorg/release/$(echo ${BOOST_VERSION} | tr _ .)/source/boost_${BOOST_VERSION}.tar.gz
RUN tar -xzf boost_${BOOST_VERSION}.tar.gz
RUN cd boost_${BOOST_VERSION}/tools/build \
  && ./bootstrap.sh \
  && ./b2 -j ${NUM_CORES} toolset=gcc install
RUN printf 'using mpi : mpiicc ;\n' > boost_${BOOST_VERSION}/tools/build/src/user-config.jam
# workaround for build of Boost.Python
# see https://svn.boost.org/trac10/ticket/11120
RUN printf 'import os ;\nlocal homepath = [ os.environ HOME ] ;\nusing python : 3.7 : /usr/bin/python3 : /usr/include/python3.7m : /usr/lib ;\n' >> \
  boost_${BOOST_VERSION}/tools/build/src/user-config.jam
RUN cd boost_${BOOST_VERSION} \
  && b2 -j ${NUM_CORES} toolset=gcc threading=multi install; exit 0
     
# Mlpack headers
RUN wget http://mlpack.org/files/mlpack-${MLPACK_VERSION}.tar.gz
RUN tar -xzf mlpack-${MLPACK_VERSION}.tar.gz
RUN cd mlpack-${MLPACK_VERSION} && mkdir mlpackbuild && cd mlpackbuild && \
  cmake -D BUILD_CLI_EXECUTABLES=OFF -D BUILD_TESTS=OFF .. && \
  make -j ${NUM_CORES} mlpack_headers && cp -r include /usr/local
  
# Bash Completion
RUN git clone https://github.com/scop/bash-completion.git
RUN cd bash-completion && autoreconf -i && ./configure && make -j ${NUM_CORES} && make install

# yaml-cpp
RUN wget https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-${YAML_VERSION}.tar.gz
RUN tar -xzf yaml-cpp-${YAML_VERSION}.tar.gz
RUN cd yaml-cpp-yaml-cpp-${YAML_VERSION} && mkdir build && cd build && cmake -D YAML_CPP_BUILD_TESTS=OFF .. && \
  make -j ${NUM_CORES} && make install

# benchmark
#RUN git clone https://github.com/google/benchmark.git
#RUN cd benchmark && mkdir build && cd build && \
#  cmake -D BENCHMARK_DOWNLOAD_DEPENDENCIES=ON -D CMAKE_BUILD_TYPE=Release -D BENCHMARK_ENABLE_LTO=true .. && \
#  make -j ${NUM_CORES} && make test && make install

## Likwid
#RUN wget http://ftp.fau.de/pub/likwid/likwid-stable.tar.gz
#RUN tar -xzf likwid-stable.tar.gz
#RUN cd likwid-* && sed -i "/COMPILER =/c\COMPILER = ICC#NO SPACE" config.mk && \
#  make -j ${NUM_CORES} && make install
#
## remove TBB
#RUN rm -rf /opt/intel/compilers_and_libraries_2019.*/linux/tbb

# additional libs for VTune gui
#RUN apt-get install -y libnss3-dev libgtk-3-0 libxss1

# Anaconda installing
#RUN wget https://repo.continuum.io/archive/Anaconda3-2020.02-Linux-x86_64.sh
#RUN bash Anaconda3-2020.02-Linux-x86_64.sh -b -p $HOME/anaconda3
#RUN rm Anaconda3-2020.02-Linux-x86_64.sh

# Updating Anaconda packages
#RUN conda update conda
#RUN conda update anaconda
#RUN conda update --all

#RUN $HOME/anaconda3/bin/conda create -n cuda_LN_PhD_py38 python=3.8 anaconda

# install locales
RUN apt-get install locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen

# Java runtime for CLion
RUN apt-get install -y default-jre

# Set working directory when docker environment starts up
#WORKDIR /

# Remove build dir
RUN rm -rf /libs

#bash as default shell
RUN chmod +x /etc/profile.d/*
RUN printf 'source etc/profile.d/*' >> ~/.bashrc
ENTRYPOINT ["/bin/bash"]

