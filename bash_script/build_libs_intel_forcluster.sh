#!/bin/bash

# How many cores to use for make (for HPC: only on the building node)
export NUM_CORES=$(( $(nproc --all) + 1))

# Installed versions of self built libraries
export PYTHON_VERSION=3.7.4
export GCC_VERSION=8.3.0
export ANACONDA_VERSION=2019.07
export CMAKE_VERSION=3.15.2
export LAPACK_VERSION=3.8.0
export ARMADILLO_VERSION=9.700.2
export TMUX_VERSION=2.9a
export NLOPT_VERSION=2.6.1
export FLEX_VERSION=2.6.4
export BISON_VERSION=3.4.1
export LIBTOOL_VERSION=2.4.6
export GLIB_VERSION=2.61.2
export LIBFFI_VERSION=3.2.1
export UTILLINUX_VERSION=2.34
export PCRE_VERSION=8.43
export GSL_VERSION=2.6
export BOOST_VERSION=1_71_0
export MLPACK_VERSION=3.1.1
export YAML_VERSION=0.6.3
export GIT_VERSION=2.24.0
export AUTOCONF_VERSION=2.69
export M4_VERSION=1.4.18
export GETTEXT_VERSION=0.20.1

if [ ! -d "${HOME}/libs_intel" ]; then
  mkdir ${HOME}/libs_intel
elif [ -d "${HOME}/libs_intel/tmp" ]; then
  rm -rf ${HOME}/libs_intel/tmp
fi
mkdir ${HOME}/libs_intel/tmp

export LD_LIBRARY_PATH="${HOME}/libs_intel/lib64:${HOME}/libs_intel/lib:${LD_LIBRARY_PATH}/"
export LIBRARY_PATH="${HOME}/libs_intel/lib64:${HOME}/libs_intel/lib:${LIBRARY_PATH}/"
export CPATH="${HOME}/libs_intel/include:${CPATH}/"
export PATH="${HOME}/libs_intel/bin:/usr/local/bin:/usr/local/sbin:/opt/moab/bin:/usr/local/bin:/usr/bin:/usr/local/sbin/:${PATH}/"
export MANPATH="${HOME}/libs_intel/share/man:${HOME}/libs_intel/man:${MANPATH}/"

# clear setup on HPC
if [ -d "/opt/bwhpc/" ]; then
  module unload compiler/intel
  module unload compiler/gnu
  module unload numlib/mkl
  module unload mpi/openmpi
  module unload mpi/impi
fi

# GCC
module load compiler/gnu/8.2

# Intel compiler
cd ${HOME}/libs_intel/tmp && wget http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/15809/parallel_studio_xe_2019_update5_cluster_edition.tgz
cd ${HOME}/libs_intel/tmp && tar -xzf parallel_studio_xe_2019_update5_cluster_edition.tgz
cd ${HOME}/libs_intel/tmp && cd parallel_studio_xe_2019_update5_cluster_edition && \
  rm silent.cfg && echo 'ACCEPT_EULA=accept
CONTINUE_WITH_OPTIONAL_ERROR=yes
PSET_INSTALL_DIR=$HOME/libs_intel/intel
CONTINUE_WITH_INSTALLDIR_OVERWRITE=yes
PSET_MODE=install
ACTIVATION_LICENSE_FILE=28518@admin2
ACTIVATION_TYPE=license_server
AMPLIFIER_SAMPLING_DRIVER_INSTALL_TYPE=kit
AMPLIFIER_DRIVER_ACCESS_GROUP=vtune
AMPLIFIER_DRIVER_PERMISSIONS=660
AMPLIFIER_LOAD_DRIVER=no
AMPLIFIER_C_COMPILER=/bin/gcc
AMPLIFIER_MAKE_COMMAND=/bin/make
AMPLIFIER_INSTALL_BOOT_SCRIPT=no
AMPLIFIER_DRIVER_PER_USER_MODE=no
INTEL_SW_IMPROVEMENT_PROGRAM_CONSENT=no
SIGNING_ENABLED=yes
ARCH_SELECTED=INTEL64
COMPONENTS=;intel-itac-common__noarch;intel-trace-analyzer__x86_64;intel-trace-collector__x86_64;intel-itac-common-pset__noarch;intel-clck__x86_64;intel-clck-support__noarch;intel-clck-pset__noarch;intel-vtune-amplifier-2019-cli-common__noarch;intel-vtune-amplifier-2019-common__noarch;intel-vtune-amplifier-2019-cli__x86_64;intel-vtune-amplifier-2019-cli-32bit__i486;intel-vtune-amplifier-2019-collector-32linux__i486;intel-vtune-amplifier-2019-collector-64linux__x86_64;intel-vtune-amplifier-2019-doc__noarch;intel-vtune-amplifier-2019-sep__noarch;intel-vtune-amplifier-2019-target__noarch;intel-vtune-amplifier-2019-gui__x86_64;intel-vtune-amplifier-2019-vpp-server__x86_64;intel-vtune-amplifier-2019-common-pset__noarch;intel-inspector-2019-cli-common__noarch;intel-inspector-2019-cli__x86_64;intel-inspector-2019-cli-32bit__i486;intel-inspector-2019-doc__noarch;intel-inspector-2019-gui__x86_64;intel-inspector-2019-cli-common-pset__noarch;intel-advisor-2019-cli-common__noarch;intel-advisor-2019-cli__x86_64;intel-advisor-2019-cli-32bit__i486;intel-advisor-2019-doc__noarch;intel-advisor-2019-fga__x86_64;intel-advisor-2019-cli-common-pset__noarch;intel-conda-index-tool__x86_64;intel-comp__x86_64;intel-comp-32bit__x86_64;intel-comp-doc__noarch;intel-comp-l-all-common__noarch;intel-comp-l-all-vars__noarch;intel-comp-nomcu-vars__noarch;intel-comp-ps-32bit__x86_64;intel-comp-ps__x86_64;intel-comp-ps-ss__x86_64;intel-comp-ps-ss-bec__x86_64;intel-comp-ps-ss-bec-32bit__x86_64;intel-openmp__x86_64;intel-openmp-32bit__x86_64;intel-openmp-common__noarch;intel-openmp-common-icc__noarch;intel-openmp-common-ifort__noarch;intel-openmp-ifort__x86_64;intel-openmp-ifort-32bit__x86_64;intel-tbb-libs-32bit__x86_64;intel-tbb-libs__x86_64;intel-idesupport-icc-common-ps__noarch;intel-conda-intel-openmp-linux-64-shadow-package__x86_64;intel-conda-intel-openmp-linux-32-shadow-package__x86_64;intel-conda-icc_rt-linux-64-shadow-package__x86_64;intel-icc__x86_64;intel-c-comp-common__noarch;intel-icc-common__noarch;intel-icc-common-ps__noarch;intel-icc-common-ps-ss-bec__noarch;intel-icc-doc__noarch;intel-icc-doc-ps__noarch;intel-icc-ps__x86_64;intel-icc-ps-ss-bec__x86_64;intel-icx__x86_64;intel-icx-common__noarch;intel-ifort__x86_64;intel-ifort-common__noarch;intel-ifort-doc__noarch;intel-mkl-common__noarch;intel-mkl-core__x86_64;intel-mkl-core-rt__x86_64;intel-mkl-doc__noarch;intel-mkl-doc-ps__noarch;intel-mkl-gnu__x86_64;intel-mkl-gnu-rt__x86_64;intel-mkl-cluster__x86_64;intel-mkl-cluster-rt__x86_64;intel-mkl-common-ps__noarch;intel-mkl-core-ps__x86_64;intel-mkl-pgi__x86_64;intel-mkl-pgi-rt__x86_64;intel-conda-mkl-linux-64-shadow-package__x86_64;intel-conda-mkl-static-linux-64-shadow-package__x86_64;intel-conda-mkl-devel-linux-64-shadow-package__x86_64;intel-conda-mkl-include-linux-64-shadow-package__x86_64;intel-mkl-common-c__noarch;intel-mkl-core-c__x86_64;intel-mkl-common-c-ps__noarch;intel-mkl-cluster-c__noarch;intel-mkl-tbb__x86_64;intel-mkl-tbb-rt__x86_64;intel-mkl-pgi-c__x86_64;intel-mkl-gnu-c__x86_64;intel-mkl-common-f__noarch;intel-mkl-core-f__x86_64;intel-mkl-cluster-f__noarch;intel-mkl-gnu-f-rt__x86_64;intel-mkl-gnu-f__x86_64;intel-mkl-f95-common__noarch;intel-mkl-f__x86_64;intel-ipp-common__noarch;intel-ipp-common-ps__noarch;intel-ipp-st__x86_64;intel-ipp-mt__x86_64;intel-ipp-st-devel__x86_64;intel-ipp-st-devel-ps__x86_64;intel-ipp-mt-devel__x86_64;intel-ipp-doc__noarch;intel-conda-ipp-linux-64-shadow-package__x86_64;intel-conda-ipp-static-linux-64-shadow-package__x86_64;intel-conda-ipp-include-linux-64-shadow-package__x86_64;intel-conda-ipp-devel-linux-64-shadow-package__x86_64;intel-tbb-devel__x86_64;intel-tbb-common__noarch;intel-tbb-doc__noarch;intel-conda-tbb-linux-64-shadow-package__x86_64;intel-conda-tbb-devel-linux-64-shadow-package__x86_64;intel-daal-core__x86_64;intel-daal-common__noarch;intel-daal-doc__noarch;intel-conda-daal-linux-64-shadow-package__x86_64;intel-conda-daal-static-linux-64-shadow-package__x86_64;intel-conda-daal-include-linux-64-shadow-package__x86_64;intel-conda-daal-devel-linux-64-shadow-package__x86_64;intel-daal-doc-ps__noarch;intel-imb__x86_64;intel-mpi-rt__x86_64;intel-mpi-sdk__x86_64;intel-mpi-doc__x86_64;intel-mpi-samples__x86_64;intel-conda-impi_rt-linux-64-shadow-package__x86_64;intel-conda-impi-devel-linux-64-shadow-package__x86_64;intel-gdb__x86_64;intel-gdb-source__noarch;intel-gdb-python-source__noarch;intel-gdb-common__noarch;intel-gdb-common-ps__noarch;intel-gdb-cdt-source__noarch;intel-icsxe__noarch;intel-psxe-common__noarch;intel-psxe-doc__noarch;intel-psxe-common-doc__noarch;intel-icsxe-doc__noarch;intel-psxe-licensing__noarch;intel-psxe-licensing-doc__noarch;intel-python3-psxe__noarch;intel-python2-psxe__noarch;intel-python-nopyver__noarch;intel-icsxe-pset
' >> silent.cfg && sed -i "/PSET_INSTALL_DIR=/c\PSET_INSTALL_DIR=$HOME/libs_intel/intel" silent.cfg
cd ${HOME}/libs_intel/tmp && cd parallel_studio_xe_2019_update5_cluster_edition && ./install.sh --silent=silent.cfg
source ${HOME}/libs_intel/intel/bin/compilervars.sh intel64
export CC=icc
export CXX=icpc
export MPICC=icc
export MPICXX=icpc
export MPIFC=ifort
export MPIF77=ifort
export MPIF90=ifort
export F77=ifort
export F90=ifort
export FC=ifort

# Python3
cd ${HOME}/libs_intel/tmp && wget ftp://sourceware.org/pub/libffi/libffi-${LIBFFI_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf libffi-${LIBFFI_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd libffi-${LIBFFI_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && tar -xJf Python-${PYTHON_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && cd Python-${PYTHON_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

#git
cd ${HOME}/libs_intel/tmp && wget https://ftp.gnu.org/gnu/m4/m4-${M4_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf m4-${M4_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd m4-${M4_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://ftp.gnu.org/gnu/autoconf/autoconf-${AUTOCONF_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf autoconf-${AUTOCONF_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd autoconf-${AUTOCONF_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://ftp.gnu.org/gnu/gettext/gettext-${GETTEXT_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf gettext-${GETTEXT_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd gettext-${GETTEXT_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://github.com/git/git/archive/v${GIT_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf v${GIT_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd git-${GIT_VERSION} && make configure && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

#CMake
cd ${HOME}/libs_intel/tmp && wget https://cmake.org/files/v$(echo ${CMAKE_VERSION} | cut -d. -f1-2)/cmake-${CMAKE_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf cmake-${CMAKE_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd cmake-${CMAKE_VERSION} && ./bootstrap --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

# TBB
# build older version 2019 U2 because of bug on ForHLR1 
# see https://software.intel.com/en-us/forums/intel-threading-building-blocks/topic/808343
cd ${HOME}/libs_intel/tmp && git clone https://github.com/wjakob/tbb.git && cd tbb && git checkout b066def
cd ${HOME}/libs_intel/tmp/tbb/build && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel .. && make -j ${NUM_CORES} && make install
rm -rf $TBBROOT
export TBBROOT=$HOME/libs_intel

# LAPACK (incl BLAS) & Armadillo
cd ${HOME}/libs_intel/tmp && wget http://www.netlib.org/lapack/lapack-${LAPACK_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf lapack-${LAPACK_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd lapack-${LAPACK_VERSION} && mkdir lapackbuild_shared && cd lapackbuild_shared && \
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel .. && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget http://sourceforge.net/projects/arma/files/armadillo-${ARMADILLO_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && tar -xJf armadillo-${ARMADILLO_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && cd armadillo-${ARMADILLO_VERSION} && mkdir armadillobuild_shared && cd armadillobuild_shared && \
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel .. && make -j ${NUM_CORES} && make install
  
# tmux
cd ${HOME}/libs_intel/tmp && wget https://github.com/tmux/tmux/releases/download/${TMUX_VERSION}/tmux-${TMUX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf tmux-${TMUX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd tmux-${TMUX_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

# NLopt
cd ${HOME}/libs_intel/tmp && wget https://github.com/stevengj/nlopt/archive/v${NLOPT_VERSION}.zip
cd ${HOME}/libs_intel/tmp && unzip v${NLOPT_VERSION}.zip
cd ${HOME}/libs_intel/tmp && cd nlopt-${NLOPT_VERSION} && mkdir build && cd build && cmake .. -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

# Doxygen
cd ${HOME}/libs_intel/tmp && wget https://github.com/westes/flex/releases/download/v${FLEX_VERSION}/flex-${FLEX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf flex-${FLEX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd flex-${FLEX_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://ftp.gnu.org/gnu/bison/bison-${BISON_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf bison-${BISON_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd bison-${BISON_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://ftp.gnu.org/gnu/libtool/libtool-${LIBTOOL_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf libtool-${LIBTOOL_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd libtool-${LIBTOOL_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && git clone https://gitlab.freedesktop.org/pkg-config/pkg-config
cd ${HOME}/libs_intel/tmp && cd pkg-config && ./autogen.sh --no-configure && ./configure --with-internal-glib --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v${UTILLINUX_VERSION}/util-linux-${UTILLINUX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf util-linux-${UTILLINUX_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd util-linux-${UTILLINUX_VERSION} && ./configure  --disable-use-tty-group --disable-makeinstall-chown --disable-makeinstall-setuid   --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget https://ftp.pcre.org/pub/pcre/pcre-${PCRE_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf pcre-${PCRE_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd pcre-${PCRE_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && wget http://ftp.gnome.org/pub/gnome/sources/glib/$(echo ${GLIB_VERSION} | cut -d. -f1-2)/glib-${GLIB_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && tar -xJf glib-${GLIB_VERSION}.tar.xz
cd ${HOME}/libs_intel/tmp && cd glib-${GLIB_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && git clone https://gitlab.com/graphviz/graphviz.git
cd ${HOME}/libs_intel/tmp && cd graphviz && ./autogen.sh && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_intel/tmp && git clone https://github.com/doxygen/doxygen.git
cd ${HOME}/libs_intel/tmp && mkdir doxygen/doxygenbuild && cd doxygen/doxygenbuild \
  && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel .. && make -j ${NUM_CORES} && make install

# GSL
cd ${HOME}/libs_intel/tmp && wget ftp://ftp.gnu.org/gnu/gsl/gsl-${GSL_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf gsl-${GSL_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd gsl-${GSL_VERSION} && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

# Google Test & Google Mock
cd ${HOME}/libs_intel/tmp && git clone https://github.com/google/googletest.git && cd ${HOME}/libs_intel/tmp \
  && cd googletest && mkdir googletestbuild && cd googletestbuild \
  && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel .. && make -j ${NUM_CORES} && make install

# Boost
cd ${HOME}/libs_intel/tmp && wget https://dl.bintray.com/boostorg/release/$(echo ${BOOST_VERSION} | tr _ .)/source/boost_${BOOST_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf boost_${BOOST_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd boost_${BOOST_VERSION}/tools/build \
  && ./bootstrap.sh \
  && ./b2 -j ${NUM_CORES} --prefix=${HOME}/libs_intel toolset=intel-19.0 install
cd ${HOME}/libs_intel/tmp && printf 'using mpi : mpiicc ;\n' > boost_${BOOST_VERSION}/tools/build/src/user-config.jam
# workaround for build of Boost.Python
# see https://svn.boost.org/trac10/ticket/11120
cd ${HOME}/libs_intel/tmp && printf 'import os ;\nlocal homepath = [ os.environ HOME ] ;\nusing python : 3.7 : $(homepath)/libs_intel/bin/python3 : $(homepath)/libs_intel/include/python3.7m : $(homepath)/libs_intel/lib ;\n' >> \
  boost_${BOOST_VERSION}/tools/build/src/user-config.jam
cd ${HOME}/libs_intel/tmp && cd boost_${BOOST_VERSION} \
  && b2 -j ${NUM_CORES} --prefix=${HOME}/libs_intel \
     toolset=intel-19.0 threading=multi install
     
# Mlpack headers
cd ${HOME}/libs_intel/tmp && wget http://mlpack.org/files/mlpack-${MLPACK_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf mlpack-${MLPACK_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd mlpack-${MLPACK_VERSION} && mkdir mlpackbuild && cd mlpackbuild && \
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel -D BUILD_CLI_EXECUTABLES=OFF -D BUILD_TESTS=OFF .. && \
  make -j ${NUM_CORES} mlpack_headers && cp -r include ${HOME}/libs_intel 
  
# Bash Completion
cd ${HOME}/libs_intel/tmp && git clone https://github.com/scop/bash-completion.git
cd ${HOME}/libs_intel/tmp && cd bash-completion && autoreconf -i && ./configure --prefix=${HOME}/libs_intel && make -j ${NUM_CORES} && make install

# yaml-cpp
cd ${HOME}/libs_intel/tmp && wget https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-${YAML_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && tar -xzf yaml-cpp-${YAML_VERSION}.tar.gz
cd ${HOME}/libs_intel/tmp && cd yaml-cpp-yaml-cpp-${YAML_VERSION} && mkdir build && cd build && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel -D YAML_CPP_BUILD_TESTS=OFF -D YAML_BUILD_SHARED_LIBS=ON .. && \
  make -j ${NUM_CORES} && make install

# benchmark
cd ${HOME}/libs_intel/tmp && git clone https://github.com/google/benchmark.git
cd ${HOME}/libs_intel/tmp && cd benchmark && mkdir build && cd build && \ 
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_intel -D BENCHMARK_DOWNLOAD_DEPENDENCIES=ON -D CMAKE_BUILD_TYPE=Release -D BENCHMARK_ENABLE_LTO=true .. && \
  make -j ${NUM_CORES} && make test && make install

# delete temp folder
rm -rf ${HOME}/libs_intel/tmp

# create intellibs.sh script
if [ -d "/opt/bwhpc/" ]; then
  rm -f ~/intellibs.sh
  printf '\n# Added by RISQ libs_intel.sh script\n' >> ~/intellibs.sh
  printf 'module unload compiler/intel\n' >> ~/intellibs.sh
  printf 'module unload compiler/gnu\n' >> ~/intellibs.sh
  printf 'module unload numlib/mkl\n' >> ~/intellibs.sh
  printf 'module unload mpi/openmpi\n' >> ~/intellibs.sh
  printf 'module unload mpi/impi\n' >> ~/intellibs.sh
  printf 'export PATH="/usr/local/bin:/usr/local/sbin:/opt/moab/bin:/usr/local/bin:/usr/bin:/usr/local/sbin/:${PATH}/"\n' >> ~/intellibs.sh
  printf 'module load compiler/gnu/8.2\n' >> ~/intellibs.sh
  printf 'export LD_LIBRARY_PATH="${HOME}/libs_intel/lib64:${HOME}/libs_intel/lib:${LD_LIBRARY_PATH}/"\n' >> ~/intellibs.sh
  printf 'export LIBRARY_PATH="${HOME}/libs_intel/lib64:${HOME}/libs_intel/lib:${LIBRARY_PATH}/"\n' >> ~/intellibs.sh
  printf 'export CPATH="${HOME}/libs_intel/include:${CPATH}/"\n' >> ~/intellibs.sh
  printf 'export PATH="${HOME}/libs_intel/bin:${PATH}/"\n' >> ~/intellibs.sh
  printf 'export MANPATH="${HOME}/libs_intel/man:${MANPATH}/"\n' >> ~/intellibs.sh
  printf 'source $HOME/libs_intel/intel/bin/compilervars.sh intel64\n' >> ~/intellibs.sh
  printf 'export TBBROOT=$HOME/libs_intel\n' >> ~/intellibs.sh
  printf 'export CC=icc\n' >> ~/intellibs.sh
  printf 'export CXX=icpc\n' >> ~/intellibs.sh
  printf 'export MPICC=icc\n' >> ~/intellibs.sh
  printf 'export MPICXX=icpc\n' >> ~/intellibs.sh
  printf 'export MPIFC=ifort\n' >> ~/intellibs.sh
  printf 'export MPIF70=ifort\n' >> ~/intellibs.sh
  printf 'export MPIF90=ifort\n' >> ~/intellibs.sh
  printf 'export F77=ifort\n' >> ~/intellibs.sh
  printf 'export F90=ifort\n' >> ~/intellibs.sh
  printf 'export FC=ifort\n' >> ~/intellibs.sh
fi
