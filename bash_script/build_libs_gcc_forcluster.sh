#!/bin/bash

# How many cores to use for make (for HPC: only on the building node)
export NUM_CORES=$(( $(nproc --all) + 1))

# Installed versions of self built libraries
export PYTHON_VERSION=3.7.4
export GCC_VERSION=9.2.0
export OPENMPI_VERSION=4.0.1
export ANACONDA_VERSION=2019.07
export CMAKE_VERSION=3.15.2
export LAPACK_VERSION=3.8.0
export OPENBLAS_VERSION=0.3.11
export ARMADILLO_VERSION=9.900.3
export TMUX_VERSION=2.9a
export NLOPT_VERSION=2.6.1
export FLEX_VERSION=2.6.4
export BISON_VERSION=3.4.1
export LIBTOOL_VERSION=2.4.6
export GLIB_VERSION=2.61.2
export LIBFFI_VERSION=3.2.1
export UTILLINUX_VERSION=2.34
export PCRE_VERSION=8.43
export GSL_VERSION=2.6
export BOOST_VERSION=1_71_0
export MLPACK_VERSION=3.1.1
export YAML_VERSION=0.6.3

if [ ! -d "${HOME}/libs_gcc" ]; then
  mkdir ${HOME}/libs_gcc
elif [ -d "${HOME}/libs_gcc/tmp" ]; then
  rm -rf ${HOME}/libs_gcc/tmp
fi
mkdir ${HOME}/libs_gcc/tmp

export LD_LIBRARY_PATH="${HOME}/libs_gcc/lib64:${HOME}/libs_gcc/lib:${LD_LIBRARY_PATH}/"
export LIBRARY_PATH="${HOME}/libs_gcc/lib64:${HOME}/libs_gcc/lib:${LIBRARY_PATH}/"
export CPATH="${HOME}/libs_gcc/include:${CPATH}/"
export PATH="${HOME}/libs_gcc/bin:${PATH}/"
export MANPATH="${HOME}/libs_gcc/man:${MANPATH}/"

# clear setup on HPC
if [ -d "/opt/bwhpc/" ]; then
  module unload compiler/intel
  module unload compiler/gnu
  module unload numlib/mkl
  module unload mpi/openmpi
  module unload mpi/impi
fi

# GCC
export CC=gcc
export CXX=g++
export OMPI_CC=gcc
export OMPI_CXX=g++
export OMPI_FC=gfortran
export OMPI_F77=gfortran
export OMPI_F90=gfortran
export F77=gfortran
export F90=gfortran
export FC=gfortran
#cd ${HOME}/libs_gcc/tmp && wget http://ftp.gnu.org/gnu/gcc/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.xz
#cd ${HOME}/libs_gcc/tmp && tar -xJf gcc-${GCC_VERSION}.tar.xz
#cd ${HOME}/libs_gcc/tmp && cd gcc-${GCC_VERSION} && ./contrib/download_prerequisites
#cd ${HOME}/libs_gcc/tmp && mkdir objdir && cd objdir && \
#  ../gcc-${GCC_VERSION}/configure --disable-multilib --enable-threads=posix --prefix=${HOME}/libs_gcc && \
#  make -j ${NUM_CORES} && make install
module load compiler/gnu/9.1
module load devel/cmake/3.17.3
module load mpi/openmpi/4.0-gnu-9.1
module load numlib/mkl/11.3.4

# Python3
# cd ${HOME}/libs_gcc/tmp && wget ftp://sourceware.org/pub/libffi/libffi-${LIBFFI_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf libffi-${LIBFFI_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd libffi-${LIBFFI_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz
# cd ${HOME}/libs_gcc/tmp && tar -xJf Python-${PYTHON_VERSION}.tar.xz
# cd ${HOME}/libs_gcc/tmp && cd Python-${PYTHON_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# OpenMPI
#cd ${HOME}/libs_gcc/tmp && wget https://download.open-mpi.org/release/open-mpi/v$(echo ${OPENMPI_VERSION} | cut -d. -f1-2)/openmpi-${OPENMPI_VERSION}.tar.gz
#cd ${HOME}/libs_gcc/tmp && tar -xzf openmpi-${OPENMPI_VERSION}.tar.gz
#cd ${HOME}/libs_gcc/tmp && cd openmpi-${OPENMPI_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

#CMake
#cd ${HOME}/libs_gcc/tmp && wget https://cmake.org/files/v$(echo ${CMAKE_VERSION} | cut -d. -f1-2)/cmake-${CMAKE_VERSION}.tar.gz
#cd ${HOME}/libs_gcc/tmp && tar -xzf cmake-${CMAKE_VERSION}.tar.gz
#cd ${HOME}/libs_gcc/tmp && cd cmake-${CMAKE_VERSION} && ./bootstrap --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# OpenBLAS
# cd ${HOME}/libs_gcc/tmp && wget https://github.com/xianyi/OpenBLAS/releases/download/v${OPENBLAS_VERSION}/OpenBLAS-${OPENBLAS_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf OpenBLAS-${OPENBLAS_VERSION}.tar.gz

# LAPACK (incl BLAS) & Armadillo
cd ${HOME}/libs_gcc/tmp && wget http://www.netlib.org/lapack/lapack-${LAPACK_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf lapack-${LAPACK_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && cd lapack-${LAPACK_VERSION} && mkdir lapackbuild_shared && cd lapackbuild_shared && \
  cmake -D BUILD_SHARED_LIBS=ON -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc .. && make -j ${NUM_CORES} && make install
cd ${HOME}/libs_gcc/tmp && wget http://sourceforge.net/projects/arma/files/armadillo-${ARMADILLO_VERSION}.tar.xz
cd ${HOME}/libs_gcc/tmp && tar -xJf armadillo-${ARMADILLO_VERSION}.tar.xz
cd ${HOME}/libs_gcc/tmp && cd armadillo-${ARMADILLO_VERSION} && mkdir armadillobuild_shared && cd armadillobuild_shared && \
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc .. && make -j ${NUM_CORES} && make install
# # tmux
# cd ${HOME}/libs_gcc/tmp && wget https://github.com/tmux/tmux/releases/download/${TMUX_VERSION}/tmux-${TMUX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf tmux-${TMUX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd tmux-${TMUX_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# neovim
cd ${HOME}/libs_gcc/tmp && wget https://github.com/neovim/neovim/archive/nightly.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf nightly.tar.gz
cd ${HOME}/libs_gcc/tmp && cd neovim-nightly && mkdir .deps && cd .deps && cmake ../third-party && make -j ${NUM_CORES}
cd ${HOME}/libs_gcc/tmp && cd neovim-nightly && mkdir build && cd build && cmake .. -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc && make CMAKE_BUILD_TYPE=Release -j ${NUM_CORES} && make install

# NLopt
cd ${HOME}/libs_gcc/tmp && wget https://github.com/stevengj/nlopt/archive/v${NLOPT_VERSION}.zip
cd ${HOME}/libs_gcc/tmp && unzip v${NLOPT_VERSION}.zip
cd ${HOME}/libs_gcc/tmp && cd nlopt-${NLOPT_VERSION} && mkdir build && cd build && cmake .. -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# pagmo
cd ${HOME}/libs_gcc/tmp && git clone https://github.com/esa/pagmo2.git
cd ${HOME}/libs_gcc/tmp && cd pagmo2 && mkdir build && cd build && cmake .. -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc -D PAGMO_WITH_EIGEN3=ON -D PAGMO_WITH_NLOPT=ON -D PAGMO_WITH_IPOPT=ON && make -j ${NUM_CORES} && make install
# # Doxygen
# cd ${HOME}/libs_gcc/tmp && wget https://github.com/westes/flex/releases/download/v${FLEX_VERSION}/flex-${FLEX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf flex-${FLEX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd flex-${FLEX_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget https://ftp.gnu.org/gnu/bison/bison-${BISON_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf bison-${BISON_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd bison-${BISON_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget https://ftp.gnu.org/gnu/libtool/libtool-${LIBTOOL_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf libtool-${LIBTOOL_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd libtool-${LIBTOOL_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && git clone https://gitlab.freedesktop.org/pkg-config/pkg-config
# cd ${HOME}/libs_gcc/tmp && cd pkg-config && ./autogen.sh --no-configure && ./configure --with-internal-glib --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v${UTILLINUX_VERSION}/util-linux-${UTILLINUX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf util-linux-${UTILLINUX_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd util-linux-${UTILLINUX_VERSION} && ./configure  --disable-use-tty-group --disable-makeinstall-chown --disable-makeinstall-setuid   --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget https://ftp.pcre.org/pub/pcre/pcre-${PCRE_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && tar -xzf pcre-${PCRE_VERSION}.tar.gz
# cd ${HOME}/libs_gcc/tmp && cd pcre-${PCRE_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && wget http://ftp.gnome.org/pub/gnome/sources/glib/$(echo ${GLIB_VERSION} | cut -d. -f1-2)/glib-${GLIB_VERSION}.tar.xz
# cd ${HOME}/libs_gcc/tmp && tar -xJf glib-${GLIB_VERSION}.tar.xz
# cd ${HOME}/libs_gcc/tmp && cd glib-${GLIB_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && git clone https://gitlab.com/graphviz/graphviz.git
# cd ${HOME}/libs_gcc/tmp && cd graphviz && ./autogen.sh && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install
# cd ${HOME}/libs_gcc/tmp && git clone https://github.com/doxygen/doxygen.git
# cd ${HOME}/libs_gcc/tmp && mkdir doxygen/doxygenbuild && cd doxygen/doxygenbuild \
#   && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc .. && make -j ${NUM_CORES} && make install

# GSL
cd ${HOME}/libs_gcc/tmp && wget ftp://ftp.gnu.org/gnu/gsl/gsl-${GSL_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf gsl-${GSL_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && cd gsl-${GSL_VERSION} && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# Google Test & Google Mock
cd ${HOME}/libs_gcc/tmp && git clone https://github.com/google/googletest.git && cd ${HOME}/libs_gcc/tmp \
  && cd googletest && mkdir googletestbuild && cd googletestbuild \
  && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc .. && make -j ${NUM_CORES} && make install

# Boost
cd ${HOME}/libs_gcc/tmp && wget https://dl.bintray.com/boostorg/release/$(echo ${BOOST_VERSION} | tr _ .)/source/boost_${BOOST_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf boost_${BOOST_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && cd boost_${BOOST_VERSION}/tools/build \
  && ./bootstrap.sh \
  && ./b2 -j ${NUM_CORES} --prefix=${HOME}/libs_gcc toolset=gcc install
cd ${HOME}/libs_gcc/tmp && printf 'using mpi : mpicc ;\n' > boost_${BOOST_VERSION}/tools/build/src/user-config.jam
# workaround for build of Boost.Python
# see https://svn.boost.org/trac10/ticket/11120
# cd ${HOME}/libs_gcc/tmp && printf 'import os ;\nlocal homepath = [ os.environ HOME ] ;\nusing python : 3.7 : $(homepath)/libs_gcc/bin/python3 : $(homepath)/libs_gcc/include/python3.7m : $(homepath)/libs_gcc/lib ;\n' >> \
#   boost_${BOOST_VERSION}/tools/build/src/user-config.jam
cd ${HOME}/libs_gcc/tmp && cd boost_${BOOST_VERSION} \
  && b2 -j ${NUM_CORES} --prefix=${HOME}/libs_gcc \
     toolset=gcc threading=multi install
     
# Mlpack headers
cd ${HOME}/libs_gcc/tmp && wget http://mlpack.org/files/mlpack-${MLPACK_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf mlpack-${MLPACK_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && cd mlpack-${MLPACK_VERSION} && mkdir mlpackbuild && cd mlpackbuild && \
  cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc -D BUILD_CLI_EXECUTABLES=OFF -D BUILD_TESTS=OFF .. && \
  make -j ${NUM_CORES} mlpack_headers && cp -r include ${HOME}/libs_gcc 
  
# Bash Completion
cd ${HOME}/libs_gcc/tmp && git clone https://github.com/scop/bash-completion.git
cd ${HOME}/libs_gcc/tmp && cd bash-completion && autoreconf -i && ./configure --prefix=${HOME}/libs_gcc && make -j ${NUM_CORES} && make install

# yaml-cpp
cd ${HOME}/libs_gcc/tmp && wget https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-${YAML_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && tar -xzf yaml-cpp-${YAML_VERSION}.tar.gz
cd ${HOME}/libs_gcc/tmp && cd yaml-cpp-yaml-cpp-${YAML_VERSION} && mkdir build && cd build && cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc -D YAML_CPP_BUILD_TESTS=OFF -D YAML_BUILD_SHARED_LIBS=ON .. && \
  make -j ${NUM_CORES} && make install

# benchmark
#cd ${HOME}/libs_gcc/tmp && git clone https://github.com/google/benchmark.git
#cd ${HOME}/libs_gcc/tmp && cd benchmark && mkdir build && cd build && \
#cmake -D CMAKE_INSTALL_PREFIX=${HOME}/libs_gcc -D BENCHMARK_DOWNLOAD_DEPENDENCIES=ON -D CMAKE_BUILD_TYPE=Release .. && \
#make -j ${NUM_CORES} && make test && make install


# delete temp folder
rm -rf ${HOME}/libs_gcc/tmp

# create gcclibs.sh script
if [ -d "/opt/bwhpc/" ]; then
  rm ~/gcclibs.sh
  printf '\n# Added by RISQ libs_gcc.sh script\n' >> ~/gcclibs.sh
  printf 'module unload compiler/intel\n' >> ~/gcclibs.sh
  printf 'module unload compiler/gnu\n' >> ~/gcclibs.sh
  printf 'module unload numlib/mkl\n' >> ~/gcclibs.sh
  printf 'module unload mpi/openmpi\n' >> ~/gcclibs.sh
  printf 'module unload mpi/impi\n' >> ~/gcclibs.sh
  printf 'export LD_LIBRARY_PATH=""\n' >> ~/gcclibs.sh
  printf 'export LIBRARY_PATH=""\n' >> ~/gcclibs.sh
  printf 'export CPATH=""\n' >> ~/gcclibs.sh
  printf 'export PATH="/software/all/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:$HOME/.local/bin/"\n' >> ~/gcclibs.sh
  printf 'export MANPATH=""\n' >> ~/gcclibs.sh
  printf 'export LD_LIBRARY_PATH="${HOME}/libs_gcc/lib64:${HOME}/libs_gcc/lib:${LD_LIBRARY_PATH}/"\n' >> ~/gcclibs.sh
  printf 'export LIBRARY_PATH="${HOME}/libs_gcc/lib64:${HOME}/libs_gcc/lib:${LIBRARY_PATH}/"\n' >> ~/gcclibs.sh
  printf 'export CPATH="${HOME}/libs_gcc/include:${CPATH}/"\n' >> ~/gcclibs.sh
  printf 'export PATH="${HOME}/libs_gcc/bin:${PATH}/"\n' >> ~/gcclibs.sh
  printf 'export MANPATH="${HOME}/libs_gcc/man:${MANPATH}/"\n' >> ~/gcclibs.sh
  printf 'export CC=gcc\n' >> ~/gcclibs.sh
  printf 'export CXX=g++\n' >> ~/gcclibs.sh
  printf 'export OMPI_CC=gcc\n' >> ~/gcclibs.sh
  printf 'export OMPI_CXX=g++\n' >> ~/gcclibs.sh
  printf 'export OMPI_FC=gfortran\n' >> ~/gcclibs.sh
  printf 'export OMPI_F70=gfortran\n' >> ~/gcclibs.sh
  printf 'export OMPI_F90=gfortran\n' >> ~/gcclibs.sh
  printf 'export F77=gfortran\n' >> ~/gcclibs.sh
  printf 'export F90=gfortran\n' >> ~/gcclibs.sh
  printf 'export FC=gfortran\n' >> ~/gcclibs.sh
  printf 'module load compiler/gnu/9.1\n' >> ~/gcclibs.sh
  printf 'module load devel/cmake/3.17.3\n' >> ~/gcclibs.sh
  printf 'module load mpi/openmpi/4.0-gnu-9.1\n' >> ~/gcclibs.sh
  printf 'module load numlib/mkl/11.3.4\n' >> ~/gcclibs.sh
fi
