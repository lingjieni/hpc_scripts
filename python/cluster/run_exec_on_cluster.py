import pickle
from enum import Enum, auto
from pathlib2 import Path
from math import floor
from util.yaml_helper import load_yaml_file
from cluster.cluster import ForCluster
from cluster.compiler import IntelWithSingularity
from cluster.mpi_command import IntelMPICommand


class CompilingOption(Enum):
    no_recompiling = auto()
    partial_recompiling = auto()  # without deleting build folder
    total_recompiling = auto()  # delete the build folder


def determin_num_of_processes_and_threads(cluster, job_id, num_of_process=None, num_of_thread=None):
    num_of_node = len(cluster.jobs_list.loc[job_id, 'NODE LIST'])
    total_num_of_proc = int(cluster.jobs_list.loc[job_id, 'PROCS'])
    if num_of_process is None and num_of_thread is None:
        num_of_process = num_of_node
        num_of_thread = int(floor(total_num_of_proc / num_of_node))
        return num_of_process, num_of_thread
    elif num_of_process is None and num_of_thread is not None:
        num_of_process = total_num_of_proc
        return num_of_process, num_of_thread
    elif num_of_process is not None and num_of_thread is None:
        num_of_thread = int(floor(total_num_of_proc / num_of_process))
        return num_of_process, num_of_thread
    else:
        # unchanged
        return num_of_process, num_of_thread


def run_exec_on_cluster(cluster, build_folder, bin_folder, build_target,
                        executable_name, compiling_option, container=None, job_id=None,
                        num_of_process=None, num_of_thread=None):
    hostfile_str = cluster.jobs_list.loc[job_id, 'NODE LIST']
    hostfile_str = ','.join(hostfile_str)

    num_of_process, num_of_thread = determin_num_of_processes_and_threads(cluster=cluster,
                                                                          job_id=job_id,
                                                                          num_of_process=num_of_process,
                                                                          num_of_thread=num_of_thread)
    # tmp = load_yaml_file(yaml_file)

    # for ele in tmp['build_target']:
    #     if ele['target'] == build_target:
    #         executable_name = ele['executable']
    compiler = IntelWithSingularity(container, build_folder, cluster)

    if compiling_option == CompilingOption.partial_recompiling:
        compiler.run_compilation(build_target, intel_env=True, clean=False,
                                 job_id=job_id)
    elif compiling_option == CompilingOption.total_recompiling:
        compiler.run_compilation(build_target, intel_env=True, clean=True,
                                 job_id=job_id)

    executable = str(Path(build_folder) / bin_folder / executable_name)
    mpi_command_intel = IntelMPICommand(np=num_of_process, hostfile=hostfile_str)
    if container is not None:
        cluster.run_mpi_in_singularity(container_loc=container, mpi_executable=executable,
                                       mpi_command=mpi_command_intel, job_id=job_id,
                                       omp_thread_num=num_of_thread,
                                       debug_output_mpi=True
                                       )
    return 0
