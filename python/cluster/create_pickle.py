import logging
import pickle
from pathlib2 import Path
from cluster.cluster import HLR1Cluster, ForCluster, UniCluster


def create_forcluster_pickle(usr, output_loc: Path = None, file_loc='forcluster.pickle', working_dir=None):
    logger = logging.getLogger(__name__)
    cluster = ForCluster(usr, working_dir=working_dir, password=password)
    logger.info(cluster.jobs_list['NODE LIST'])
    if output_loc is not None:
        file_loc_str = str(output_loc / file_loc)
    else:
        file_loc_str = str(file_loc)
    with open(file_loc_str, 'wb') as file:
        pickle.dump(cluster, file)


def create_hlr1cluster_pickle(usr, output_loc: Path = None, file_loc='hlr1.pickle', working_dir=None):
    logger = logging.getLogger(__name__)
    cluster = HLR1Cluster(usr, working_dir=working_dir)
    logger.info(cluster.jobs_list['NODE LIST'])
    if output_loc is not None:
        file_loc_str = str(output_loc / file_loc)
    else:
        file_loc_str = str(file_loc)
    with open(file_loc_str, 'wb') as file:
        pickle.dump(cluster, file)


def create_unicluster_pickle(usr, output_loc: Path = None, file_loc='unicluster.pickle', working_dir=None):
    logger = logging.getLogger(__name__)
    cluster = UniCluster(usr, working_dir=working_dir)
    logger.info(cluster.jobs_list['NODE LIST'])
    # print(cluster.jobs_list['NODE LIST'])
    if output_loc is not None:
        file_loc_str = str(output_loc / file_loc)
    else:
        file_loc_str = str(file_loc)
    with open(file_loc_str, 'wb') as file:
        pickle.dump(cluster, file)
