import os
from pathlib2 import Path
import logging
from cluster.cluster import ForCluster, HLR1Cluster
from typing import List
import copy


class Compiler:
    def __init__(self, build_folder, project_folder, cluster=None) -> None:
        self.build_folder = str(build_folder)
        self.cluster_obj = cluster
        self.project_folder = str(project_folder)

    def run_cmake(self, **kwargs):
        profiling_option = ''
        arch_option = ''
        job_id = None
        for key, value in kwargs.items():
            if key == 'profiling' and value is True:
                profiling_option = '-DTIME_PROFILING=ON'
            if key == 'clean' and value is True:
                clean_command = ['rm', '-rf', self.build_folder]
                self.exec(clean_command, job_id)
            if key == 'job_id':
                job_id = value
            if key == 'arch' and value is not None:
                arch_option = '-DARCH=' + value

        create_dir_command = ['mkdir', '-p', self.build_folder]
        self.exec(command=create_dir_command, job_id=job_id)
        # otherwise it modifies the self.env in somewhere
        compiler_env = copy.deepcopy(kwargs.get('compiler_env', ['']))
        if compiler_env != ['']:
            compiler_env.append('&&')
            cmake_command = compiler_env + ['cmake', '-S', self.project_folder, '-B', self.build_folder,
                                            profiling_option, arch_option]
        else:
            cmake_command = ['cmake', '-S', self.project_folder, '-B', self.build_folder, profiling_option]

        self.exec(command=cmake_command, job_id=job_id)

    def run_make(self, build_target, **kwargs):
        job_id = kwargs.get('job_id', None)
        make_command = ['make', '-C', str(self.build_folder), '-j$(( $(nproc --all) - 1))']
        build_abs_dir = Path(os.path.expandvars(str(self.build_folder)))
        if not Path.exists(build_abs_dir):
            logger = logging.getLogger(__name__)
            logger.error("build folder {} does not exist, run cmake first!".format(str(self.build_folder)))
            raise ValueError("build folder {} does not exist, run cmake first!".format(str(self.build_folder)))
        if isinstance(build_target, str):  # single str or list of str
            make_command.append(build_target)
        else:
            make_command.extend(build_target)

        # otherwise it modifies the self.env in somewhere
        compiler_env = copy.deepcopy(kwargs.get('compiler_env', ['']))
        if compiler_env != ['']:
            compiler_env.append('&&')
            make_command = compiler_env + make_command
        self.exec(command=make_command, job_id=job_id)

    def run_compilation(self, build_target, **kwargs):
        # build folder cloud be changed into some sub folder
        # profiling_option = '-DENABLE_PROFILING=OFF'
        profiling_option = ''
        job_id = None
        for key, value in kwargs.items():
            if key == 'profiling' and value is True:
                profiling_option = '-DTIME_PROFILING=ON'
            if key == 'clean' and value is True:
                clean_command = ['rm', '-rf', self.build_folder]
                self.cluster.run(clean_command)
            if key == 'job_id':
                job_id = value
        create_dir_command = ['mkdir', '-p', self.build_folder]
        self.exec(command=create_dir_command, job_id=job_id)
        compiler_env = kwargs.get('compiler_env', [''])
        make_command = ['make', '-C', self.build_folder, '-j$(( $(nproc --all) - 1))']
        if isinstance(build_target, str):  # single str or list of str
            make_command.append(build_target)
        else:
            make_command.extend(build_target)
        if compiler_env != ['']:
            compiler_env.append('&&')
            cmake_command = compiler_env + ['cmake', '-S', self.project_folder, '-B', self.build_folder,
                                            profiling_option]
            make_command = compiler_env + make_command
        else:
            cmake_command = ['cmake', '-S', self.project_folder, '-B', self.build_folder, profiling_option]

        self.exec(command=cmake_command, job_id=job_id)
        self.exec(command=make_command, job_id=job_id)

    def exec(self, command: List[str], job_id):
        self.cluster_obj.run(command, job_id=job_id)


class ComplierOnLogin(Compiler):

    def exec(self, command: List[str], job_id):
        self.cluster_obj.c_login.run(command=' '.join(command))


class IntelWithSingularity(Compiler):
    def __init__(self, sif_container, build_folder, project_folder, cluster=None) -> None:
        super().__init__(build_folder, project_folder, cluster)
        self.sif_container = sif_container

    # @SingularityContainer.in_container
    def run_compilation(self, build_target, **kwargs):
        compiler_env = ['']
        try:
            if kwargs['intel_env']:
                compiler_env = ['source', '/opt/intel/bin/compilervars.sh', 'intel64']
            else:
                compiler_env = ['']
        except KeyError:
            compiler_env = ['']
        super().run_compilation(build_target, compiler_env=compiler_env, **kwargs)

    def exec(self, command, job_id):
        self.cluster_obj.exec_in_singularity(container_loc=self.sif_container, command=command, job_id=job_id)


class IntelCompiler(Compiler):

    def __init__(self, build_folder, project_folder, cluster=None) -> None:
        super().__init__(build_folder, project_folder, cluster)

    def run_compilation(self, build_target, **kwargs):
        kwargs.get('compiler_env', [self.source_intellibs])
        super().run_compilation(build_target, compiler_env=self.cluster_obj.compiler_env_intel, **kwargs)


class IntelCompilerOnLogin(IntelCompiler):

    def exec(self, command: List[str], job_id):
        self.cluster_obj.c_login.run(command=' '.join(command))
