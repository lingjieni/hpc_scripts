import os
import re
import pandas as pd
import logging
import hostlist
from typing import List
import time, datetime
from subprocess import Popen
from enum import Enum, auto
from fabric import Connection
from invoke import Context
from cluster.mpi_command import *
from util.prase_time import try_parsing_time


class ClusterName(Enum):
    local = auto()
    hlr1 = auto()
    forcluster = auto()
    unicluster = auto()


def run_with_subprocess(command):
    p = Popen(args=command)
    p.wait()


class JobListCols(Enum):
    USERNAME = auto()
    STATE = auto()
    PROCS = 'PROCS'
    REMAINING = 'REMAINING'
    STARTTIME = auto()
    NODE_LIST = 'NODE LIST'
    INTERACTIVE = 'INTERACTIVE'


class LocalMachine:
    def __init__(self) -> None:
        self.c_local = Context()

    def run(self, command: List[str], job_id=None, **kwargs):
        command = ' '.join(command)
        result = self.c_local.run(command, **kwargs)
        return result

    def run_mpi(self, command: List[str], **kwargs):
        mpi_command = ['mpirun', '-n', '3']
        mpi_command += command
        # run_with_subprocess(mpi_command)
        command = ' '.join(mpi_command)
        self.c_local.run(command, **kwargs)

    @property
    def compiler_env_intel(self):
        return ['']

    def rsync(self, local, dst, exclude_dir=None):
        command = "rsync -avzh"
        exclude_command = "" if exclude_dir is None else " ".join(["--exclude", exclude_dir])
        command = " ".join([command, exclude_command, str(local), str(dst)])
        return self.c_local.run(command)


class Cluster:
    def __init__(self, host, user, node_id=None, working_dir=None, password=None) -> None:
        # check if on login node
        self.host = host
        self.user = user

        self.on_login_node = False
        self.on_cluster = False
        self.interactive_nodes = {}

        if password is not None:
            self.c_login: Connection = Connection(self.host, self.user, connect_kwargs={"password": password})
        else:
            self.c_login: Connection = Connection(self.host, self.user)
        self.c_main = None
        self.c_interacitve = None
        self.best_interactive_job = None

        self.determine_location()
        with self.c_login as c_active:
            self.jobs_list = self.create_job_nodelist_dict(c=c_active)
            if working_dir is None:
                self.working_dir = Path(c_active.run('echo $HOME', hide=True).stdout.rstrip())
            else:
                command = ['echo', str(working_dir)]
                self.working_dir = Path(c_active.run(' '.join(command)).stdout.rstrip())
        # print(self.jobs_list.columns)
        logger = logging.getLogger(__name__)
        logger.info('working dir: {}'.format(self.working_dir))
        # since we need non thread-locked object for serilization
        self.init_non_locked_connection(node_id)

    def init_non_locked_connection(self, node_id=None):
        logger = logging.getLogger(__name__)
        self.c_login = Connection(self.host, self.user)
        if node_id is not None:
            if self.on_login_node:
                self.c_interacitve = Connection(node_id, user=self.user)
            elif not self.on_cluster:
                self.c_interacitve = Connection(node_id, user=self.user, gateway=self.c_login)
        elif not self.jobs_list.empty:
            for job_id, job in self.jobs_list.iterrows():
                if job['INTERACTIVE'] is True:
                    nodes = job['NODE LIST']
                    self.interactive_nodes[job_id] = []
                    for node in nodes:
                        if self.on_cluster:
                            self.interactive_nodes[job_id].append(Connection(node, user=self.user))
                        else:
                            self.interactive_nodes[job_id].append(
                                Connection(node, user=self.user, gateway=self.c_login))
        self.c_main = self.determine_main(node_id=node_id)
        logger.info("modify_interactive_job_id connection {}".format(self.c_main))

    def determine_main(self, node_id=None):
        c_main = None
        if self.on_cluster and self.on_login_node and self.interactive_nodes:
            if node_id:
                self.c_main = self.c_interacitve
            elif self.interactive_nodes:
                self.best_interactive_job, c_main = self.get_best_interactive_node()
        elif self.on_cluster and not self.on_login_node:
            self.best_interactive_job, _ = self.get_best_interactive_node()
            c_main = Context()
        elif not self.on_cluster:
            if self.interactive_nodes:
                self.best_interactive_job, c_main = self.get_best_interactive_node()
            else:
                c_main = self.c_login
        return c_main

    @staticmethod
    def extract_nodelist(job_nodes_list):
        if any(re.findall(r',|-', job_nodes_list, re.IGNORECASE)):
            job_nodes_list = job_nodes_list.split(":")
            nodes = []
            for item in job_nodes_list:
                nodes += hostlist.expand_hostlist(item)
        else:
            # single node case
            nodes = re.findall(r'\[([A-Za-z0-9_]+)\]', job_nodes_list)
        return nodes

    def get_node_list(self, job_id):
        result = self.jobs_list.loc[job_id, 'NODE LIST']
        return result

    # @property
    # def check_avail_resource_command(self):
    #     raise NotImplementedError('command not defined')

    def check_avail_resource(self) -> None:
        print('available resources: ')
        self.c_login.run(self.check_avail_resource_command, warn=True)

    def run(self, command: List[str], job_id=None, in_dir=None, **kwargs):
        logger = logging.getLogger(__name__)
        if isinstance(command, str):
            command = [command]
        env = kwargs.pop('env', None)
        if env is not None:
            for key, value in env.items():
                export_envs = ['export', key + '=' + value, '&&']
                command = export_envs + command

        command = ' '.join(command)
        logger.info('run command %s', command)
        if job_id is not None:
            c_first_in_interactive = self.interactive_nodes[job_id][0]
            if in_dir is not None:
                with c_first_in_interactive.cd(in_dir):
                    c_first_in_interactive.run('pwd', **kwargs)
                    result = c_first_in_interactive.run(command, **kwargs)
            else:
                result = c_first_in_interactive.run(command, **kwargs)
        else:
            result = self.c_main.run(command, **kwargs)
        return result

    def run_mpi(self, mpi_executable: str, mpi_command: MPICommand,
                job_id=None, in_dir=None, trace_with_itac=None, omp_thread_num=None,
                allocate_natively=False, mpi_pin_domain="socket", omp_places="cores",
                omp_proc_bind="spread",
                debug_output_mpi=True, **kwargs):
        logger = logging.getLogger(__name__)

        # since singularity does not start with bash, otherwise the mpi lib will not be loaded! For IntelMPI
        if job_id is not None:
            mpi_command.hostfile = ','.join(self.get_node_list(job_id=job_id))
            logger.info(self.interactive_nodes[job_id][0])
            # logger.info(self.jobs_list.loc[job_id, 'NODE TYPE'])
        if trace_with_itac:
            mpi_executable = ' '.join(
                ['export LD_PRELOAD=libVT.so', '&&', 'source /opt/intel/parallel_studio_xe_2019.4.070/psxevars.sh',
                 '&&', mpi_executable])
        if omp_thread_num is not None:
            mpi_executable = ' '.join(
                ['export OMP_NUM_THREADS=' + str(omp_thread_num), '&&', mpi_executable]
            )

        mpi_executable = self.wrapper_with_bash(mpi_executable)

        if allocate_natively:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, debug_output_mpi=debug_output_mpi,
                                             allocate_natively=True)
        else:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, mpi_pin_domain=mpi_pin_domain,
                                             omp_places=omp_places, omp_proc_bind=omp_proc_bind,
                                             debug_output_mpi=debug_output_mpi, allocate_natively=False)
        commands_list = [self.load_pre_installed_mpi(mpi_command), '&&', mpi_option, mpi_executable]
        # start = '# >>> mpi running  >>>'
        # end = '# <<< mpi running  <<<'
        # For OpenMPI
        # self.write_loading2bashrc(start=start, end=end, content=self.load_pre_installed_mpi(mpi_command=mpi_command))
        logger.debug('run mpi', ' '.join(commands_list))
        self.run(command=commands_list, job_id=job_id, in_dir=in_dir, **kwargs)
        # self.clear_content_in_bashrc(start=start, end=end)
        return 0

    def get_job_node_arch(self, job_id):
        raise NotImplementedError

    def rsync2node(self, local, dst, node_id, exclude_dir=None):
        command = "rsync -avzhe ssh"
        exclude_command = "" if exclude_dir is None else " ".join(["--exclude", exclude_dir])
        target_loc = ":".join([node_id, dst])
        command = " ".join([command, exclude_command, local, target_loc])
        return self.c_login.run(command)

    def rsync(self, local, dst, exclude_dir=None):
        command = "rsync -avzh"
        exclude_command = "" if exclude_dir is None else " ".join(["--exclude", exclude_dir])
        command = " ".join([command, exclude_command, str(local), str(dst)])
        return self.c_login.run(command)

    def submit_job(self, bash_script, command_params=None, remote_folder=None):
        logger = logging.getLogger(__name__)
        logger.info('uploading script')
        self.c_login.put(str(bash_script), remote=str(remote_folder))
        logger.info('Uploading finised')
        command = self.submit_command + ' ' + command_params + ' ' + str(bash_script)
        self.run(command)

    def wrapper_with_omp(self, command):
        return 'export NUM_OF_THREADS'

    def wrapper_with_bash(self, command):
        return '/bin/bash -c ' + "'" + command + "'"

    # @contextmanager
    # def cd(self, path):
    #     self.c_main.cd(path)

    def showjobs(self, c=None, hide=True, **kwargs):
        _c = self.c_login if c is None else c
        return _c.run(self.showjobs_command, hide=True, **kwargs)

    def check_job(self, job_id, c=None, **kwargs):
        pass

    def create_job_nodelist_dict(self, c=None) -> pd.DataFrame:
        return pd.DataFrame()

    def get_active_jobs(self, c=None) -> pd.DataFrame:
        pass

    def get_job_info(self, job_id, c=None):
        """
        finished job infomation
        :param job_id:
        :param c:
        :return:
        """
        # _c = self.c_login if c is None else c
        try:
            info_id = self.jobs_list.loc[job_id]
        except KeyError as e:
            logger = logging.getLogger(__name__)
            logger.error('no job with id {} found in current list, please update cluster pickle'.format(job_id))
            raise e
        return info_id

    def get_num_of_cores_per_node(self, job_id) -> int:
        # num of cores with hyper-threading
        pass

    def get_num_of_nodes(self, job_id):
        interactive_jobs = self.jobs_list[self.jobs_list['INTERACTIVE'] == True]
        num_of_nodes = int(interactive_jobs.loc[job_id, 'NODES'])
        return num_of_nodes

    def create_hostfile(self, path2hostfile='./hostfile'):
        if self.jobs_list.empty or self.best_interactive_job is None:
            raise ValueError('get an interacive job first!')
        # best_job_node_list = self.jobs_list[self.jobs_list['JOBID'] == self.best_interactive_job]['NODE LIST'].values[0]
        interactive_jobs = self.jobs_list[self.jobs_list['INTERACTIVE'] == True]
        for job_id, job in interactive_jobs.iterrows():
            print(job_id, job['NODE LIST'])
            with open(str(Path(path2hostfile) / (job_id + '_hostfile')), 'w') as f:
                for item in job['NODE LIST']:
                    f.write('%s\n' % item)

    def time2second(self, time_str):
        if str.startswith(time_str, '-'):
            return 0
        else:
            try:
                tmp = time.strptime(time_str, '%H:%M:%S')
                sec = datetime.timedelta(hours=tmp.tm_hour, minutes=tmp.tm_min,
                                         seconds=tmp.tm_sec).total_seconds()
                return sec
            except ValueError:
                try:
                    tmp = time.strptime(time_str, '%d:%H:%M:%S')
                    sec = datetime.timedelta(days=tmp.tm_mday, hours=tmp.tm_hour, minutes=tmp.tm_min,
                                             seconds=tmp.tm_sec).total_seconds()
                    return sec
                except Exception as e:
                    logger = logging.getLogger(__name__)
                    logger.error(e)

    def get_best_interactive_node(self):
        # best means here hardware, remaining time and must be interactive
        if not self.jobs_list.empty:
            time_in_sec = self.jobs_list[self.jobs_list['INTERACTIVE'] == True]['REMAINING'].apply(self.time2second)
            longest_remaining_time_id = time_in_sec.idxmax()
            if self.interactive_nodes:
                return longest_remaining_time_id, self.interactive_nodes[longest_remaining_time_id][0]
            else:
                raise ValueError('get an interacive job first')
        else:
            logger = logging.getLogger(__name__)
            logger.info("no interactive job avaiable")
            return None , None

    def put(self, file, path2put):
        if self.on_cluster:
            self.c_main.run('cp ' + file + ' ' + path2put)
        else:
            self.c_main.put(local=file, remote=path2put)

    def determine_location(self):
        try:
            if self.cluster_name == os.environ['CLUSTER']:
                self.on_cluster = True
                if self.login_hostname == os.environ['HOSTNAME']:
                    self.on_login_node = True
                    # no matter where it is
                    # c_main = c_login
                else:
                    self.on_login_node = False
                    # c_main = Context()
            else:
                self.on_cluster = False
            #     raise ValueError
        except KeyError:
            self.on_cluster = False
            # c_main = c_login
        # return c_main

    def drop_num_of_cores_in_str(self, original_node_list_str):
        result = re.sub('\*[0-9][0-9]', '', original_node_list_str)
        result = re.sub('\:[0-9][0-9]', '', result)
        return result

    def load_pre_installed_mpi(self, mpi_command):
        if isinstance(mpi_command, OpenMPICommand):
            return 'source ~/gcclibs.sh'
        elif isinstance(mpi_command, IntelMPICommand):
            return 'source ~/intellibs.sh'

    @property
    def submit_command(self):
        return None

    @property
    def cluster_name(self):
        return None

    @property
    def login_hostname(self):
        return None

    @property
    def compiler_env_intel(self):
        return ['source ~/intellibs.sh']

    def vtune_command(self, executable, **kwargs):
        # locale_isssue = "export LC_ALL='en_US.UTF-8'"
        vtume_vars = 'source /opt/intel/vtune_amplifier/amplxe-vars.sh'
        options = '-collect hotspots -trace-mpi'
        result_dir = '-r ' + kwargs['result_dir']
        amplxe_cl_command = " ".join(['amplxe-cl', options, result_dir, executable])
        return " && ".join([vtume_vars, amplxe_cl_command])


class MoabHPC:
    @property
    def check_avail_resource_command(self):
        showbf = 'showbf -f'
        return showbf

    @property
    def checkjob_command(self):
        checkjob = 'checkjob'
        return checkjob

    @property
    def showjobs_command(self):
        return "showq"


class SlurmHPC:
    @property
    def check_avail_resource_command(self):
        return 'sinfo_t_idle'

    @property
    def showjobs_command(self):
        return "squeue"

    @property
    def checkjob_command(self):
        checkjob = 'scontrol show job'
        return checkjob

    @staticmethod
    def _get_active_jobs(current_job_info, **kwargs) -> pd.DataFrame:
        # tmp = self.showjobs(hide=True)
        tmp_in_lines = list(filter(lambda x: x.strip() != '', current_job_info.stdout.splitlines()))
        keys = tmp_in_lines[0].split()
        jobs_list = pd.DataFrame(columns=keys)
        indices_of_jobs = tmp_in_lines[1:]
        # indices_of_jobs = [i for i, s in enumerate(tmp_in_lines[1:])]
        for val in indices_of_jobs:
            jobs_list = jobs_list.append(dict(zip(keys, val.split())), ignore_index=True)
        # print(jobs_list['ST'] == 'R')
        # print(jobs_list['NAME'] == 'sh')
        # print(jobs_list['ST'] == 'R' and jobs_list['NAME'] == 'sh')
        return jobs_list[(jobs_list['ST'] == 'R') & (jobs_list['NAME'] == 'sh')]

    def _create_job_nodelist_dict(self) -> pd.DataFrame:
        pass
        # logger = logging.getLogger(__name__)
        # logger.info('getting ids of active jobs...')
        # job_list = self.get_active_jobs()
        # job_node_list = []
        # num_of_procs_list = []
        # remaining_time_list = []
        # for index, job in job_list.iterrows():
        #     job_id = job['JOBID']
        #     tmp = self.check_job(job_id, hide=True).stdout.split()
        #     run_time = [x for x in tmp if re.search('RunTime', x)][0].split('=')[1]
        #     run_time = try_parsing_time(run_time)
        #     time_limit = [x for x in tmp if re.search('TimeLimit', x)][0].split('=')[1]
        #     time_limit = try_parsing_time(time_limit)
        #     remaining_time_list.append(str(time_limit - run_time))
        #     num_of_procs = int([x for x in tmp if re.search('NumCPUs', x)][0].split('=')[1])
        #     num_of_procs_list.append(num_of_procs)
        #     node_list = job['NODELIST(REASON)']
        #     job_node_list.append(self.extract_nodelist(node_list))
        #
        # job_list[JobListCols.PROCS.value] = num_of_procs_list
        # job_list[JobListCols.INTERACTIVE.value] = True
        # job_list[JobListCols.REMAINING.value] = remaining_time_list
        # job_list = job_list.astype({JobListCols.PROCS.value: 'int', JobListCols.INTERACTIVE.value: 'bool'})
        # job_list.rename({'NODELIST(REASON)': JobListCols.NODE_LIST.value}, axis=1, inplace=True)
        # job_list[JobListCols.NODE_LIST.value] = job_node_list
        # logger.info('getting details of jobs...')
        # job_list = job_list.set_index('JOBID')
        # return job_list


class InteractiveNode:
    def __init__(self, node_id) -> None:
        super().__init__()


class ForCluster(Cluster, MoabHPC):
    def __init__(self, user, node_id=None, serilizeable=False, container_loc=None, working_dir=None,
                 password=None) -> None:
        # host = 'bwforcluster.bwservices.uni-heidelberg.de'
        host = 'bwfor.cluster.uni-mannheim.de'
        super().__init__(host, user, node_id, working_dir, password)
        self.container_loc = container_loc
        # force the login on forcluster to be login1, in order to keep the $TMP folder for compilation constantly
        if password is not None:
            self.c_login = Connection('login1', self.user, gateway=self.c_login, connect_kwargs={"password": password})
        else:
            self.c_login = Connection('login1', self.user, gateway=self.c_login)

    def check_avail_resource(self) -> None:
        list_of_resources = {'standard': 'standard', 'old gpu': 'gpu', 'new gpu': 'gpu-sky',
                             'coprocessor': 'mic', 'old best': 'best',
                             'new best': 'best-sky'}
        for key, value in list_of_resources.items():
            print('==============')
            print(key.upper())
            split_line = '==============\n'
            name = key.upper() + '\n'
            command = self.check_avail_resource_command
            command += " " + value
            result = self.c_login.run(command, warn=True)

    def get_num_of_cores_per_node(self, job_id):
        interactive_jobs = self.jobs_list[self.jobs_list['INTERACTIVE'] == True]
        num_of_nodes = int(interactive_jobs.loc[job_id, 'NODES'])
        procs = int(interactive_jobs.loc[job_id, 'PROCS'])
        return int(procs / num_of_nodes)

    def showjobs(self, hide=False, **kwargs):
        return self.c_login.run(self.showjobs_command, hide=hide, **kwargs)

    def check_job(self, job_id, **kwargs):
        return self.c_login.run(self.checkjob_command + ' ' + job_id, **kwargs)

    def unload_mpi_gnu(self):
        self.c_main.run('module list')

    def create_job_nodelist_dict(self, **kwargs) -> pd.DataFrame:
        logger = logging.getLogger(__name__)
        logger.info('getting ids of active jobs...')
        job_list = self.get_active_jobs()
        job_node_list = []
        interactives = []
        node_types = []
        num_of_nodes = []
        for index, job in job_list.iterrows():
            job_id = job['JOBID']
            node_list, interactive, node_type = self.get_job_details(job_id)
            interactives.append(interactive)
            node_types.append(node_type)
            node_list = self.extract_nodelist(node_list)
            job_node_list.append(node_list)
            num_of_nodes.append(len(node_list))

        job_list['NODE LIST'] = job_node_list
        job_list['INTERACTIVE'] = interactives
        job_list['NODE TYPE'] = node_types
        job_list['NODES'] = num_of_nodes
        logger.info('getting details of jobs...')
        job_list = job_list.set_index('JOBID')
        return job_list

    def get_active_jobs(self, c=None):
        _c = self.c_login if c is None else c
        tmp = self.showjobs(hide=True)
        tmp_in_lines = list(filter(lambda x: x.strip() != '', tmp.stdout.splitlines()))
        indices = [i for i, s in enumerate(tmp_in_lines) if 'active job' in s]
        key_pos = indices[0] + 1
        jobs_list = pd.DataFrame(columns=tmp_in_lines[key_pos].split())
        for val_pos in range(indices[0] + 2, indices[1]):
            jobs_list = jobs_list.append(
                dict(zip(tmp_in_lines[key_pos].split(), tmp_in_lines[val_pos].split())),
                ignore_index=True)
        return jobs_list

    def get_job_details(self, job_id):
        job_details = list(filter(lambda x: x.strip() != '', self.check_job(job_id, hide=True).stdout.splitlines()))
        pos_allocated_nodes = [i for i, s in enumerate(job_details) if 'Allocated Nodes' in s]
        pos_flags = [i for i, s in enumerate(job_details) if 'Flags' in s]
        interactive = True if job_details[pos_flags[0]].find('INTERACTIVE') != -1 else False
        pos_node_type = [i for i, s in enumerate(job_details) if 'Applied Nodeset' in s]
        node_type = job_details[pos_node_type[0]].replace('Applied Nodeset: ', '')
        if node_type == 'gpu-sky':
            node_list = re.sub('\:[0-9]', '', job_details[pos_allocated_nodes[0] + 1])
        else:
            node_list = self.drop_num_of_cores_in_str(job_details[pos_allocated_nodes[0] + 1])
        return node_list, interactive, node_type

    def exec_in_singularity(self, container_loc, command, job_id=None, omp_thread_num=None):
        if omp_thread_num is not None:
            command = 'export OMP_NUM_THREADS=' + str(omp_thread_num) + ' && ' + command
        command = self.wrapper_with_bash(' '.join(command))
        command = [self.singularity, 'exec', container_loc, command]
        self.run(command, job_id=job_id)

    def run_mpi_in_singularity(self, container_loc: str, mpi_executable: str, mpi_command: MPICommand,
                               job_id=None, in_dir=None, trace_with_itac=None, omp_thread_num=None,
                               allocate_natively=False, mpi_pin_domain="socket", omp_places="cores",
                               omp_proc_bind="spread",
                               debug_output_mpi=True):
        logger = logging.getLogger(__name__)
        # since singularity does not start with bash, otherwise the mpi lib will not be loaded! For IntelMPI
        if job_id is not None:
            mpi_command.hostfile = ','.join(self.get_node_list(job_id=job_id))
            logger.info(self.interactive_nodes[job_id][0])
            logger.info(self.jobs_list.loc[job_id, 'NODE TYPE'])
        if trace_with_itac:
            mpi_executable = ' '.join(
                ['export LD_PRELOAD=libVT.so', '&&', 'source /opt/intel/parallel_studio_xe_2019.4.070/psxevars.sh',
                 '&&', mpi_executable])
        if omp_thread_num is not None:
            mpi_executable = ' '.join(
                ['export OMP_NUM_THREADS=' + str(omp_thread_num), '&&', mpi_executable]
            )

        mpi_executable = self.wrapper_with_bash(mpi_executable)

        if allocate_natively:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, debug_output_mpi=debug_output_mpi)
        else:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, mpi_pin_domain=mpi_pin_domain,
                                             omp_places=omp_places, omp_proc_bind=omp_proc_bind,
                                             debug_output_mpi=debug_output_mpi)

        if allocate_natively:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, debug_output_mpi=debug_output_mpi)
        else:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, mpi_pin_domain=mpi_pin_domain,
                                             omp_places=omp_places, omp_proc_bind=omp_proc_bind,
                                             debug_output_mpi=debug_output_mpi)

        commands_list = [self.load_pre_installed_mpi(mpi_command), '&&',
                         mpi_option,
                         self.singularity,
                         'exec', container_loc,
                         mpi_executable]
        logger.debug('run mpi in singularity', ' '.join(commands_list))
        # start = '# >>> mpi running  >>>'
        # end = '# <<< mpi running  <<<'
        # start = '# >>> mpi running  >>>'
        # end = '# <<< mpi running  <<<'
        # For OpenMPI
        # self.write_loading2bashrc(start=start, end=end, content=self.load_pre_installed_mpi(mpi_command=mpi_command))
        self.run(command=commands_list, job_id=job_id, in_dir=in_dir)
        # self.clear_content_in_bashrc(start=start, end=end)

    def profiling_mpi_in_singularity_with_vtune(self, container_loc: str, mpi_executable: str, mpi_command: MPICommand,
                                                result_dir: str, job_id=None, omp_thread_num=None,
                                                mpi_pin_domain="socket", omp_places="cores",
                                                omp_proc_bind="spread", debug_output_mpi=True):
        mpi_executable = self.vtune_command(mpi_executable, result_dir=result_dir)
        self.run_mpi_in_singularity(container_loc, mpi_executable, mpi_command, job_id, omp_thread_num=omp_thread_num,
                                    mpi_pin_domain=mpi_pin_domain, omp_places=omp_places,
                                    omp_proc_bind=omp_proc_bind, debug_output_mpi=debug_output_mpi)

    def trace_mpi_in_singularity_with_itac(self, container_loc: str, mpi_executable: str, mpi_command: MPICommand,
                                           result_dir: str, job_id=None, omp_thread_num=None,
                                           mpi_pin_domain="socket", omp_places="cores", omp_proc_bind="spread",
                                           debug_output_mpi=True):
        # mpi_executable_list = [mpi_executable]
        # mpi_executable = ' '.join(mpi_executable_list)
        self.c_main.run(' '.join(['mkdir', '-p', result_dir]))
        self.run_mpi_in_singularity(container_loc, mpi_executable, mpi_command, job_id, result_dir,
                                    trace_with_itac=True, omp_thread_num=omp_thread_num,
                                    mpi_pin_domain=mpi_pin_domain, omp_places=omp_places,
                                    omp_proc_bind=omp_proc_bind, debug_output_mpi=debug_output_mpi)
        return 0

    def run_likwid_mpi_in_singularity(self, container_loc: str, mpi_executable: str, mpi_command: MPICommand):
        # since singularity does not start with bash, otherwise the mpi lib will not be loaded! For IntelMPI
        likwid_command = 'likwid-perfctr -c S0:1  -g BRANCH '
        mpi_executable = self.wrapper_with_bash(likwid_command + mpi_executable)
        commands_list = [self.load_pre_installed_mpi(mpi_command), '&&', mpi_command.command(), self.singularity,
                         'exec', container_loc, mpi_executable]
        command = " ".join(commands_list)
        start = '# >>> mpi running  >>>'
        end = '# <<< mpi running  <<<'
        # For OpenMPI
        self.write_loading2bashrc(start=start, end=end, content=self.load_pre_installed_mpi(mpi_command=mpi_command))
        try:
            self.c_main.run(command)
        except Exception as e:
            # For OpenMPI
            self.clear_content_in_bashrc(start=start, end=end)
            raise e

    def get_job_node_arch(self, job_id):
        node_type = self.get_job_info(job_id=job_id)['NODE TYPE']
        if node_type == 'best-sky':
            node_arch = 'skylake'
        else:
            node_arch = 'haswell'
        return node_arch

    @property
    def singularity(self):
        return 'singularity'

    @property
    def cluster_name(self):
        return 'mlswiso'

    def load_pre_installed_mpi(self, mpi_command):
        if isinstance(mpi_command, OpenMPICommand):
            return 'source ~/gcclibs.sh'
        elif isinstance(mpi_command, IntelMPICommand):
            return 'source ~/intellibs.sh'
        # if isinstance(mpi_command, OpenMPICommand):
        #     return 'module load compiler/gnu/9.1 && module load mpi/openmpi/4.0-gnu-9.1'
        # elif isinstance(mpi_command, IntelMPICommand):
        #     return 'module load compiler/intel/19.0 && module load mpi/impi/2019-intel-19.0'

    def write_loading2bashrc(self, start, end, content):
        write2bashrc = start + '\n' + content + '\n' + end
        write2bashrc = "'" + write2bashrc + "'"
        self.c_login.run(' echo ' + write2bashrc + ' >> ~/.bashrc')

    def clear_content_in_bashrc(self, start, end):
        self.c_login.run('sed -i ' + "'/" + start + "/" + ',' + "/" + end + "/d'" + ' ~/.bashrc')


class HLR1Cluster(Cluster, SlurmHPC):
    def __init__(self, user, working_dir=None) -> None:
        host = 'forhlr1.scc.kit.edu'
        super().__init__(host, user, working_dir=working_dir)

    def check_avail_resource(self) -> None:
        self.c_login.run(self.check_avail_resource_command)

    def showjobs(self, **kwargs):
        return self.c_login.run(self.showjobs_command, **kwargs)

    def get_num_of_cores_per_node(self, job_id):
        return 20

    def create_job_nodelist_dict(self, c=None) -> pd.DataFrame:
        logger = logging.getLogger(__name__)
        logger.info('getting ids of active jobs...')
        job_list = self.get_active_jobs(c=c)
        job_node_list = []
        num_of_procs_list = []
        remaining_time_list = []
        for index, job in job_list.iterrows():
            job_id = job['JOBID']
            tmp = self.check_job(job_id, c=c, hide=True).stdout.split()
            run_time = [x for x in tmp if re.search('RunTime', x)][0].split('=')[1]
            run_time = try_parsing_time(run_time)
            time_limit = [x for x in tmp if re.search('TimeLimit', x)][0].split('=')[1]
            time_limit = try_parsing_time(time_limit)
            time_delta = time_limit - run_time
            day, hours, mins, seconds = time_delta.days, time_delta.seconds // 3600, time_delta.seconds // 60 % 60, time_delta.seconds % 60
            if time_delta.days == 0:
                remaining_time_list.append('{:02}:{:02}:{:02}'.format(hours, mins, seconds))
            else:
                remaining_time_list.append('{}:{:02}:{:02}:{:02}'.format(day, hours, mins, seconds))

            num_of_procs = int([x for x in tmp if re.search('NumCPUs', x)][0].split('=')[1])
            num_of_procs_list.append(num_of_procs)
            node_list = job['NODELIST(REASON)']
            job_node_list.append(self.extract_nodelist(node_list))

        job_list[JobListCols.PROCS.value] = num_of_procs_list
        job_list[JobListCols.INTERACTIVE.value] = True
        job_list[JobListCols.REMAINING.value] = remaining_time_list
        job_list = job_list.astype({JobListCols.PROCS.value: 'int', JobListCols.INTERACTIVE.value: 'bool'})
        job_list.rename({'NODELIST(REASON)': JobListCols.NODE_LIST.value}, axis=1, inplace=True)
        job_list[JobListCols.NODE_LIST.value] = job_node_list
        logger.info('getting details of jobs...')
        job_list = job_list.set_index('JOBID')
        return job_list

    def get_active_jobs(self, c=None) -> pd.DataFrame:
        tmp = self.showjobs(hide=True)
        tmp_in_lines = list(filter(lambda x: x.strip() != '', tmp.stdout.splitlines()))
        keys = tmp_in_lines[0].split()
        jobs_list = pd.DataFrame(columns=keys)
        indices_of_jobs = tmp_in_lines[1:]
        # indices_of_jobs = [i for i, s in enumerate(tmp_in_lines[1:])]
        for val in indices_of_jobs:
            jobs_list = jobs_list.append(dict(zip(keys, val.split())), ignore_index=True)
        # print(jobs_list['ST'] == 'R')
        # print(jobs_list['NAME'] == 'sh')
        # print(jobs_list['ST'] == 'R' and jobs_list['NAME'] == 'sh')
        return jobs_list[(jobs_list['ST'] == 'R') & (jobs_list['NAME'] == 'sh')]

    def check_job(self, job_id, c=None, **kwargs):
        pipe_command = kwargs.pop('pipe', ' ')
        return self.c_login.run(self.checkjob_command + ' ' + job_id + pipe_command, **kwargs)

    def run_mpi(self, mpi_executable: str, mpi_command: MPICommand,
                job_id=None, in_dir=None, trace_with_itac=None, omp_thread_num=None,
                allocate_natively=False, mpi_pin_domain="socket", omp_places="cores",
                omp_proc_bind="spread",
                debug_output_mpi=True, **kwargs):

        logger = logging.getLogger(__name__)
        # since singularity does not start with bash, otherwise the mpi lib will not be loaded! For IntelMPI
        if job_id is not None and mpi_command.hostfile is None:
            mpi_command.hostfile = ','.join(self.get_node_list(job_id=job_id))
            logger.info(self.interactive_nodes[job_id][0])
            # logger.info(self.jobs_list.loc[job_id, 'NODE TYPE'])
        if trace_with_itac:
            mpi_executable = ' '.join(
                ['export LD_PRELOAD=libVT.so', '&&', 'source /opt/intel/parallel_studio_xe_2019.4.070/psxevars.sh',
                 '&&', mpi_executable])
        if omp_thread_num is not None:
            mpi_executable = ' '.join(
                ['export OMP_NUM_THREADS=' + str(omp_thread_num), '&&', mpi_executable]
            )

        mpi_executable = self.wrapper_with_bash(mpi_executable)

        if allocate_natively:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, debug_output_mpi=debug_output_mpi,
                                             allocate_natively=True)
        else:
            mpi_option = mpi_command.command(trace_with_itac=trace_with_itac, mpi_pin_domain=mpi_pin_domain,
                                             omp_places=omp_places, omp_proc_bind=omp_proc_bind,
                                             debug_output_mpi=debug_output_mpi, allocate_natively=False)
        commands_list = [self.load_pre_installed_mpi(mpi_command), '&&', mpi_option, mpi_executable]
        # start = '# >>> mpi running  >>>'
        # end = '# <<< mpi running  <<<'
        # For OpenMPI
        # self.write_loading2bashrc(start=start, end=end, content=self.load_pre_installed_mpi(mpi_command=mpi_command))
        logger.debug('run mpi', ' '.join(commands_list))
        self.run(command=commands_list, job_id=job_id, in_dir=in_dir, **kwargs)
        # self.clear_content_in_bashrc(start=start, end=end)
        return 0

    def load_pre_installed_mpi(self, mpi_command):
        if isinstance(mpi_command, OpenMPICommand):
            return 'source ~/gcclibs.sh'
        elif isinstance(mpi_command, IntelMPICommand):
            return 'source ~/intellibs.sh'

    @property
    def showjobs_command(self):
        return super().showjobs_command

    @property
    def cluster_name(self):
        return 'fh1'

    @property
    def login_hostname(self):
        return ['fh1n989.localdomain']

    @property
    def submit_command(self):
        return 'sbatch'


class UniCluster(Cluster, SlurmHPC):
    def __init__(self, user, working_dir=None) -> None:
        super().__init__(host='bwunicluster.scc.kit.edu', user=user, working_dir=working_dir)

    def check_avail_resource(self) -> None:
        super().check_avail_resource()

    def get_active_jobs(self, c=None) -> pd.DataFrame:
        _c = self.c_login if c is None else c
        return SlurmHPC._get_active_jobs(self.showjobs(hide=True, c=_c))

    def create_job_nodelist_dict(self, c=None) -> pd.DataFrame:
        logger = logging.getLogger(__name__)
        logger.info('getting ids of active jobs...')
        job_list = self.get_active_jobs(c=c)
        job_node_list = []
        num_of_procs_list = []
        remaining_time_list = []
        for index, job in job_list.iterrows():
            job_id = job['JOBID']
            tmp = self.check_job(job_id, c=c, hide=True).stdout.split()
            run_time = [x for x in tmp if re.search('RunTime', x)][0].split('=')[1]
            run_time = try_parsing_time(run_time)
            time_limit = [x for x in tmp if re.search('TimeLimit', x)][0].split('=')[1]
            time_limit = try_parsing_time(time_limit)
            time_delta = time_limit - run_time
            day, hours, mins, seconds = time_delta.days, time_delta.seconds // 3600, time_delta.seconds // 60 % 60, time_delta.seconds % 60
            if time_delta.days == 0:
                remaining_time_list.append('{:02}:{:02}:{:02}'.format(hours, mins, seconds))
            else:
                remaining_time_list.append('{}:{:02}:{:02}:{:02}'.format(day, hours, mins, seconds))

            num_of_procs = int([x for x in tmp if re.search('NumCPUs', x)][0].split('=')[1])
            num_of_procs_list.append(num_of_procs)
            node_list = job['NODELIST(REASON)']
            job_node_list.append(self.extract_nodelist(node_list))

        job_list[JobListCols.PROCS.value] = num_of_procs_list
        job_list[JobListCols.INTERACTIVE.value] = True
        job_list[JobListCols.REMAINING.value] = remaining_time_list
        job_list = job_list.astype({JobListCols.PROCS.value: 'int', JobListCols.INTERACTIVE.value: 'bool'})
        job_list.rename({'NODELIST(REASON)': JobListCols.NODE_LIST.value}, axis=1, inplace=True)
        job_list[JobListCols.NODE_LIST.value] = job_node_list
        logger.info('getting details of jobs...')
        job_list = job_list.set_index('JOBID')
        return job_list

    def check_job(self, job_id, c=None, **kwargs):
        pipe_command = kwargs.pop('pipe', ' ')
        return self.c_login.run(self.checkjob_command + ' ' + job_id + pipe_command, **kwargs)

    def get_num_of_cores_per_node(self, job_id) -> int:
        interactive_jobs = self.jobs_list[self.jobs_list['INTERACTIVE'] == True]
        num_of_nodes = int(interactive_jobs.loc[job_id, 'NODES'])
        procs = int(interactive_jobs.loc[job_id, 'PROCS'])
        return int(procs / num_of_nodes)


class OnForClusterLogin(ForCluster):
    def __init__(self, user, node_id=None, serilizeable=False, container_loc=None, working_dir=None,
                 password=None) -> None:
        self.user = user
        self.c_main = None
        self.c_interacitve = None
        self.best_interactive_job = None
        self.c_login = Context()
        self.interactive_nodes = {}

        self.determine_location()
        self.jobs_list = self.create_job_nodelist_dict(c=self.c_login)
        if working_dir is None:
            self.working_dir = Path(self.c_login.run('echo $HOME', hide=True).stdout.rstrip())
        else:
            command = ['echo', str(working_dir)]
            self.working_dir = Path(self.c_login.run(' '.join(command)).stdout.rstrip())
        # print(self.jobs_list.columns)
        logger = logging.getLogger(__name__)
        logger.info('working dir: {}'.format(self.working_dir))
        # since we need non thread-locked object for serilization
        self.init_non_locked_connection(node_id)

    def check_avail_resource(self) -> None:
        list_of_resources = {'standard': 'standard', 'old gpu': 'gpu', 'new gpu': 'gpu-sky',
                             'coprocessor': 'mic', 'old best': 'best',
                             'new best': 'best-sky'}
        for key, value in list_of_resources.items():
            print('==============')
            print(key.upper())
            split_line = '==============\n'
            name = key.upper() + '\n'
            command = self.check_avail_resource_command
            command += " " + value
            result = self.c_login.run(command, warn=True)

    def init_non_locked_connection(self, node_id=None):
        logger = logging.getLogger(__name__)
        if node_id is not None:
            if self.on_login_node:
                self.c_interacitve = Connection(node_id, user=self.user)
            else:
                raise ValueError("wrong self.on_login_node")
        elif not self.jobs_list.empty:
            for job_id, job in self.jobs_list.iterrows():
                if job['INTERACTIVE'] is True:
                    nodes = job['NODE LIST']
                    self.interactive_nodes[job_id] = []
                    for node in nodes:
                        if self.on_cluster:
                            self.interactive_nodes[job_id].append(Connection(node, user=self.user))
                        else:
                            self.interactive_nodes[job_id].append(
                                Connection(node, user=self.user, gateway=self.c_login))
        self.c_main = self.determine_main(node_id=node_id)


class OnUniClusterLogin(UniCluster):
    def __init__(self, user, node_id=None, serilizeable=False, container_loc=None, working_dir=None,
                 password=None) -> None:
        self.user = user
        self.c_main = None
        self.c_interacitve = None
        self.best_interactive_job = None
        self.c_login = Context()
        self.interactive_nodes = {}

        self.determine_location()
        self.jobs_list = self.create_job_nodelist_dict(c=self.c_login)
        if working_dir is None:
            self.working_dir = Path(self.c_login.run('echo $HOME', hide=True).stdout.rstrip())
        else:
            command = ['echo', str(working_dir)]
            self.working_dir = Path(self.c_login.run(' '.join(command)).stdout.rstrip())
        # print(self.jobs_list.columns)
        logger = logging.getLogger(__name__)
        logger.info('working dir: {}'.format(self.working_dir))
        # since we need non thread-locked object for serilization
        self.init_non_locked_connection(node_id)

    def check_avail_resource(self) -> None:
        super().check_avail_resource()

    def init_non_locked_connection(self, node_id=None):
        logger = logging.getLogger(__name__)
        if node_id is not None:
            if self.on_login_node:
                self.c_interacitve = Connection(node_id, user=self.user)
            else:
                raise ValueError("wrong self.on_login_node")
        elif not self.jobs_list.empty:
            for job_id, job in self.jobs_list.iterrows():
                if job['INTERACTIVE'] is True:
                    nodes = job['NODE LIST']
                    self.interactive_nodes[job_id] = []
                    for node in nodes:
                        if self.on_cluster:
                            self.interactive_nodes[job_id].append(Connection(node, user=self.user))
                        else:
                            self.interactive_nodes[job_id].append(
                                Connection(node, user=self.user, gateway=self.c_login))
        self.c_main = self.determine_main(node_id=node_id)
