from pathlib2 import Path


class MPICommand:
    def __init__(self, np, hostfile=None) -> None:
        self.np = str(np)
        if hostfile:
            self._hostfile = hostfile
            # if not Path(hostfile).exists():
            #     raise ValueError('no hostfile exists!')

    def command(self, **kwargs):
        pass


class OpenMPICommand(MPICommand):

    def __init__(self, np, hostfile=None) -> None:
        super().__init__(np, hostfile=hostfile)

    def command(self, **kwargs):
        if self._hostfile:
            commands_list = [self.mpi_command, '-np', self.np, '--hostfile', self.hostfile, self.mpi_flags]
        else:
            commands_list = [self.mpi_command, '-np', self.np, self.mpi_flags]
        command = ' '.join(commands_list)
        return command

    @property
    def mpi_command(self):
        return 'mpirun'

    @property
    def mpi_flags(self):
        # Todo take into account omp_places, omp_proc_bind, debug_output_mpi, mpi_pin_domain
        return '-map-by ppr:1:socket -bind-to socket -report-bindings'


class IntelMPICommand(MPICommand):

    def __init__(self, np, hostfile=None) -> None:

        super().__init__(np, hostfile=hostfile)

    def command(self, **kwargs):
        commands_list = [self.mpi_command]
        if kwargs.get('trace_with_itac', False):
            commands_list.append('-trace')
        if self.hostfile:
            num_of_hosts = len(self.hostfile.split(','))
            num_of_proc_per_host = str(int(round(int(self.np) / num_of_hosts)))
            if int(self.np) % num_of_hosts == 0:
                commands_list.extend([
                    '-hosts', self.hostfile,
                    '-perhost', num_of_proc_per_host,
                ])
            else:
                commands_list.extend([
                    '-hosts', self.hostfile,
                    '-np', self.np,
                    '-ppn', num_of_proc_per_host
                ])
        else:
            commands_list.extend(['-np', self.np])

        if not(kwargs.get('allocate_natively', False)):
            commands_list.extend([self.get_mpi_flags(mpi_pin_domain=kwargs.get('mpi_pin_domain', 'auto'),
                                                     omp_places=kwargs.get('omp_places', 'cores'),
                                                     omp_proc_bind=kwargs.get('omp_proc_bind', 'spread'))])

        commands_list.extend(['-genv', 'I_MPI_DEBUG=' + '5' if kwargs.get('debug_output_mpi', 'False') else '0'])

        command = ' '.join(commands_list)
        return command

    @property
    def mpi_command(self):
        return 'mpirun'

    def get_mpi_flags(self, mpi_pin_domain, omp_places, omp_proc_bind):
        #Todo: update with intel vars
        #see https://software.intel.com/en-us/cpp-compiler-developer-guide-and-reference-thread-affinity-interface-linux-and-windows
        commands_list = []
        commands_list.extend(['-genv', 'I_MPI_PIN_DOMAIN=' + mpi_pin_domain])
        commands_list.extend(['-genv', 'OMP_PLACES=' + omp_places])
        commands_list.extend(['-genv', 'OMP_PROC_BIND=' + omp_proc_bind])
        #Todo: make configurable via switch
        commands_list.extend(['-genv', 'FI_PROVIDER=' + 'verbs'])
        mpi_flags = ' '.join(commands_list)
        return mpi_flags

    @property
    def hostfile(self):
        return self._hostfile

    @hostfile.setter
    def hostfile(self, hostfile):
        self._hostfile = hostfile
