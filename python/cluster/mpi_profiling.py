class LIKWIDCommand():
    def __init__(self) -> None:
        super().__init__()

    @property
    def topology_command(self):
        return 'likwid-topology'
