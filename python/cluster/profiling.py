class Profiling:
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def vtune_command(executable, **kwargs):
        vtume_vars = 'source /opt/intel/vtune_amplifier/amplxe-vars.sh'
        options = '-collect hotspots -trace-mpi'
        result_dir = '-r ' + kwargs['result_dir']
        amplxe_cl_command = " ".join(['amplxe-cl', options, result_dir, executable])
        return " && ".join([vtume_vars, amplxe_cl_command])

    @staticmethod
    def vtune_command_deco(func):
        def inner(executable, **kwargs):
            vtume_vars = 'source /opt/intel/vtune_amplifier/amplxe-vars.sh'
            options = '-collect hotspots -trace-mpi'
            result_dir = '-r ' + kwargs['result_dir']
            amplxe_cl_command = " ".join(['amplxe-cl', options, result_dir, executable])
            result = " && ".join([vtume_vars, amplxe_cl_command])
            func(result, result_dir)

        return inner

    @property
    def itac_trace_command(self):
        return '-trace'
