import logging

from enum import Enum


class LogLevelCPP(Enum):
    debug = 1
    info = 2


def ask_for_logging_level():
    level = input("logging level?({}, {})".format(LogLevelCPP.debug, LogLevelCPP.info))
    level_enum = LogLevelCPP[level]
    cmake_arg = ''
    if level_enum == LogLevelCPP.debug:
        # cmake_arg = '-DSPDLOG_ACTIVE_LEVEL=1'
        cmake_arg = '-DLOG_DEBUGCUDA'
        print('lets try')
    elif level_enum == LogLevelCPP.info:
        cmake_arg = ''
        print('lets try info ')
    else:
        print('something is wrong')
    return cmake_arg


if __name__ == "__main__":
    ask_for_logging_level()