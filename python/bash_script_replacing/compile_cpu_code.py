import sys, errno
import subprocess
from pathlib import Path

from bash_script_replacing.check_MOAB_variables import *

sys.path.append(os.path.dirname(__file__))


def compile_cpu_code(cpu_target, build_type, project_dir, cluster, par_type=None):
    MOAB_JOBNAME = None
    MOAB_PROCOUNNT = None
    if cluster == 'local_machine':
        try:
            os.environ['CUDACXX']
        except Exception as e:
            os.environ['CUDACXX'] = '/usr/local/cuda-9.2/bin/nvcc'
            logging.info('set CUDACXX to {}'.format(os.environ['CUDACXX']))
        build_dir = project_dir / ('build_local-' + build_type)
        MOAB_JOBNAME = check_MOAB_variable(ENV_var='MOAB_JOBNAME', value_if_not_existed='INTERACTIVE')
        MOAB_PROCOUNNT = check_MOAB_variable(ENV_var='MOAB_PROCOUNNT', value_if_not_existed='2')
    else:
        build_dir = project_dir / 'build'
        MOAB_JOBNAME = check_MOAB_variable(ENV_var='MOAB_JOBNAME', value_if_not_existed='INTERACTIVE')
        MOAB_PROCOUNNT = check_MOAB_variable(ENV_var='MOAB_PROCOUNNT', value_if_not_existed='16')
    try:
        # print(project_dir/"build")
        os.makedirs(build_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    num_of_process_4_compiling = int(3 / 2 * int(MOAB_PROCOUNNT))

    try:
        cmake_args = []
        if cluster != 'local_machine':
            home_dir = Path(os.environ['HOME'])
            cmake_loc = home_dir / 'libs' / 'bin' / 'cmake'
        else:
            cmake_loc = 'cmake'
        cmake_args.append(cmake_loc)
        logging.info('Using cmake from {}'.format(cmake_loc))
        if par_type == 'mpi':
            cmake_args.append('-DENABLE_MPI=ON')
        # cmake_args.append(ask_for_logging_level())
        if cluster == 'local_machine':
            if 'HOSTNAME' in os.environ:
                if os.getenv('HOSTNAME') == 'fbv-cram-research':
                    cmake_args.append('-DCMAKE_C_COMPILER=/opt/rh/devtoolset-7/root/bin/gcc')
                    cmake_args.append('-DCMAKE_CXX_COMPILER=/opt/rh/devtoolset-7/root/bin/g++')
            if build_type == 'debug':
                cmake_args.append('-DCMAKE_BUILD_TYPE=Debug')
            elif build_type == 'release':
                cmake_args.append('-DCMAKE_BUILD_TYPE=Release')
            else:
                logging.warning('No build type specified!')
        cmake_args.append('..')
        cmake_args.append('-DCPU_ONLY=OFF')
        # if check_if_gpu_available():
        #     cmake_args.append('-DCPU_ONLY=OFF')
        # else:
        #     cmake_args.append('-DCPU_ONLY=ON')

        logging.info('Set logging level in c++ at '.format(cmake_args))
        # arg =
        os.chdir(build_dir)
        # print('where am I: ', os.getcwd())
        logging.info('Run {} in {}'.format(cmake_loc, os.getcwd()))
        p_cmake = subprocess.Popen(cmake_args)
        if p_cmake.wait() == 0:
            print()
        else:
            raise RuntimeError
        # make_args = []
        # print(make_args)
        make_args = ['make', cpu_target, '-j', str(num_of_process_4_compiling)]
        logging.info('Make target {} with {}'.format(cpu_target, num_of_process_4_compiling))
        p_make = subprocess.Popen(make_args)
        if p_make.wait() == 0:
            print()
        else:
            raise RuntimeError
    except Exception as e:
        logging.warning('Compilation did NOT finish correctly')
        raise
    return build_dir
