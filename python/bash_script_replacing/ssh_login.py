import paramiko
import warnings
# warnings.filterwarnings(action='ignore',module='.*paramiko.*')
ssh = paramiko.SSHClient()

ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

hostname = 'bwfor.cluster.uni-mannheim.de'
username = 'ka_hd7013'
private_key = '/home/ln/.ssh/id_rsa'
ssh.connect(hostname, username=username, key_filename=private_key)

stdin, stdout, stderr = ssh.exec_command('ls')

print(stdout.readlines())

ssh.close()
