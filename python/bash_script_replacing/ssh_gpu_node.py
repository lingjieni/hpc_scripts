# Created by ln at 26.04.19
import logging
import subprocess


def send_command_via_ssh2node(p_ssh, command):
    print("test")


def get_name_of_running_node():
    showq_args = ['showq', '-r']
    p_showq = subprocess.Popen(showq_args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    key_pos = None
    job_pos = None
    out_str_list = []
    while p_showq.poll() is None:
        out_str = p_showq.stdout.readline().decode()
        out_str_list.append(out_str)

    for i in range(len(out_str_list)):
        if out_str_list[i].startswith('active jobs'):
            key_pos = i + 1
            job_pos = key_pos + 2

    key_list = out_str_list[key_pos].split()
    active_jb_list = out_str_list[job_pos].split()
    active_jb_dict = dict(zip(key_list, active_jb_list))
    return active_jb_dict['MHOST']


def exec_in_gpu_node(container_loc, container, exec_loc, exec):
    node_id = get_name_of_running_node()
    logging.info('exec code in node %s', node_id)
    ssh2node_args = ['ssh', '-T', node_id]
    p_ssh2node = subprocess.Popen(ssh2node_args,
                                  stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE,
                                  universal_newlines=True,
                                  bufsize=0
                                  )
    # p_ssh2node.stdin.write("ls .\n")
    p_ssh2node.stdin.write("cd " + container_loc + "\n")
    p_ssh2node.stdin.write("singularity shell --nv " + container + "\n")
    p_ssh2node.stdin.write("cd " + exec_loc + "\n")
    p_ssh2node.stdin.write("pwd\n")
    p_ssh2node.stdin.write("./" + exec + "\n")
    p_ssh2node.stdin.close()
    for line in p_ssh2node.stdout:
        if line == "END\n":
            break
        print(line, end="")
    #
    for line in p_ssh2node.stdout:
        if line == "END\n":
            break
        print(line, end="")


if __name__ == '__main__':
    import util.location_determination as sb
    project_dir = sb.project_dir
    cluster = sb.cluster
