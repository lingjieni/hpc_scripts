import os
import subprocess
import sys
import logging

sys.path.append(os.path.dirname(__file__))
from bash_script_replacing.get_list_of_files import *

from enum import Enum


class ProfilingType(Enum):
    nvvp = 1
    nvprof = 2


def ask_profiling_type():
    y_or_n = input("Do you want export output(y/n)?")
    if y_or_n == 'y':
        return ProfilingType.nvvp
    elif y_or_n == 'n':
        return ProfilingType.nvprof
    else:
        raise Exception


def specify_location_and_out_put():
    return 0


def profile_gpu_code(path2exe, gpu_exe, profile_output, cluster):
    profiling_type = ask_profiling_type()
    if profiling_type == ProfilingType.nvvp:
        _profile_nvvp_gpu_code(path2exe=path2exe, gpu_exe=gpu_exe, profile_output=profile_output)
    elif profiling_type == ProfilingType.nvprof:
        _profile_nvprof_gpu_code(path2exe=path2exe, gpu_exe=gpu_exe)


def _profile_nvvp_gpu_code(path2exe, gpu_exe, profile_output):
    location_of_exe = search_file(path2exe, gpu_exe)
    if location_of_exe is None:
        logging.error('no executable in folder {}'.format(path2exe))
        raise Exception

    # if(cluster !='mlswiso'):
    #     # print('wrong cluster!')
    #     raise Exception('wrong cluster!')
    # else:
    try:
        gpu_profile = 'nvprof'
        # options =
        arg = [gpu_profile, '-f', '-o', str(path2exe / profile_output),
               str(location_of_exe)]
        # print(arg)
        p_gpu = subprocess.Popen(arg)
        p_gpu.wait()
    except Exception as ex:
        print(ex)
        print('wrong cluster')
    return 0


def _profile_nvprof_gpu_code(path2exe, gpu_exe):
    location_of_exe = search_file(path2exe, gpu_exe)
    if location_of_exe is None:
        logging.error('no executable in folder {}'.format(path2exe))
        raise Exception

    # if(cluster !='mlswiso'):
    #     # print('wrong cluster!')
    #     raise Exception('wrong cluster!')
    # else:
    try:
        gpu_profile = 'nvprof'
        # options =
        arg = [gpu_profile, str(location_of_exe)]
        # print(arg)
        p_gpu = subprocess.Popen(arg)
        p_gpu.wait()
    except Exception as ex:
        print(ex)
        print('wrong cluster')
    return 0
