import warnings
from paramiko import SSHClient, AutoAddPolicy
from scp import SCPClient
import sys
import logging
from getpass import getpass


# useful with multiple threads to track source
def _progress4(filename, size, sent, peername):
    # logging.info("(%s:%s) %s\'s progress: %.2f%%   \r" % (peername[0], peername[1], filename, float(sent) / float(size) * 100))
    sys.stdout.write(
        "(%s:%s) %s\'s progress: %.2f%%   \r" % (peername[0], peername[1], filename, float(sent) / float(size) * 100))


def upload_file2cluster(file, remote_path, ask_pass=False):
    warnings.filterwarnings(action='ignore', module='.*paramiko.*')
    ssh = SSHClient()
    key_file = '/home/ln/.ssh/id_rsa'
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    # ssh.load_system_host_keys()
    hostname = 'bwfor.cluster.uni-mannheim.de'

    username = 'ka_hd7013'
    ssh.load_system_host_keys()
    if ask_pass:
        password = getpass()
        ssh.connect(hostname, username=username, password=password)
    else:
        ssh.connect(hostname, username=username, key_filename=key_file)
    # private_key = '/home/ln/.ssh/id_rsa'
    # ssh.connect(hostname, username=username, key_filename=private_key)

    # SCPCLient takes a paramiko transport as an argument
    scp = SCPClient(ssh.get_transport(), progress4=_progress4)

    scp.put(file, recursive=True, remote_path=str(remote_path))
    logging.info('upload file to '+ str(remote_path))
    scp.close()
    ssh.close()


if __name__ == '__main__':
    import util.location_determination as sb

    project_dir = sb.project_dir
    cluster = sb.cluster
    file = 'test_from_scp'
    remote_path = '/home/ka/ka_fbv/ka_hd7013'
    upload_file2cluster(file, remote_path)
