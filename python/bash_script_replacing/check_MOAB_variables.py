import os
import logging


def check_MOAB_variable(ENV_var, value_if_not_existed):
    try:
        value = os.environ[ENV_var]
        if value == '':
            os.environ[ENV_var] = value_if_not_existed
            logging.info('{} is empty string and set to {}'.format(ENV_var, value_if_not_existed))
        # logging.info('set {} to value {}'.format(ENV_var, value_if_not_existed))
    except Exception as e:
        logging.info('{} does not exists. Set to {}'.format(e, value_if_not_existed))
        # logging.info('set {} to value {}'.format(e, value_if_not_existed))
        os.environ[ENV_var] = value_if_not_existed
        value = value_if_not_existed
    return value
