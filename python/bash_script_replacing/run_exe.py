from subprocess import Popen, PIPE, STDOUT


def run_catch_test(test_exe, test_case):
    return 0


def run_exe(executable):
    args = [executable]
    p = Popen(args)
    p.wait()


def mpi_run_exe(cpu_exe, num_of_processes):
    args = ['mpirun', '-np', str(num_of_processes), cpu_exe]
    p = Popen(args)
    p.wait()


def mpi_run_catch_exe(cpu_exe, test_name, num_of_processes):
    args = ['mpirun', '-np', str(num_of_processes), cpu_exe, test_name]
    p = Popen(args)
    p.wait()


def profile_with_valrind(executable):
    args = ['valgrind', '--tool=callgrind', executable]
    p = Popen(args)
    p.wait()
