import os, errno
import logging
import subprocess


def exec_in_singularity(container, command: list, nv=False):
    """

    :param container:
    :param command:
    :param nv: if the option for nvidia
    :return:
    """
    # sing represents singularity
    sing_args = []
    sing_loc = 'singularity'
    sing_command = 'exec'
    sing_args.append(sing_loc)
    sing_args.append(sing_command)
    if nv:
        logging.info('-nv active')
        sing_args.append('--nv')
    sing_container = container
    sing_args.append(sing_container)
    sing_args.extend(command)
    p_singularity_shell = subprocess.Popen(sing_args)
    p_singularity_shell.wait()


def compile_with_singularity(project_dir, container, target):
    print("complie_with_sigularity")
    cmake = 'cmake'
    logging.info('Using cmake from {}'.format(cmake))
    build_dir = project_dir / 'build_singularity'
    target_dir = build_dir / 'bin'
    # make dir if not exists
    try:
        os.makedirs(build_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    # go to build dir
    os.chdir(build_dir)
    cmakelist_loc = '..'
    cmake_args = [cmake, cmakelist_loc, '-DWithinSingularity=ON']
    logging.info('Run {} in {}'.format(cmake, os.getcwd()))
    logging.info('Set logging level in c++ at '.format(cmake_args))
    cmake_command = cmake_args
    sing_container = str(project_dir / container)
    exec_in_singularity(sing_container, cmake_command, nv=False)
    make_args = ['make', target, '-j', '2']
    make_command = make_args
    # p_pwd = subprocess.Popen('pwd')
    # p_pwd.wait()
    exec_in_singularity(sing_container, make_command, nv=False)
    return build_dir, target_dir


if __name__ == '__main__':
    import util.location_determination as sb
    project_name = 'cuda_LN_PhD'
    project_dir, cluster = sb.get_project_location(project_name)
    container = 'sif_files/cuda_dev.sif'
    compile_with_singularity(project_dir, container, 'test-pf_seq_vs_par_simulated_data')
