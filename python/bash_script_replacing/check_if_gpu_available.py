

def ask_cpu_only():
    y_or_n = input("Do you want only cpu version (y/n)?")
    if y_or_n == 'y':
        return True
    elif y_or_n == 'n':
        return False
    else:
        raise Exception

def check_if_gpu_available():
    cpu_only = ask_cpu_only()
    return not cpu_only
