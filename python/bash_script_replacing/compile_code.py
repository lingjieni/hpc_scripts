class CompileCode(object):
    cmake_loc, cmake_args = None
    make_loc, make_args = None

    def __init__(self) -> None:
        """
        define make cmake and compile
        """
        pass

    def compile(self):
        pass


class ExecLocation(object):
    def __init__(self) -> None:
        super().__init__()


class GPUSide(object):
    def __init__(self) -> None:
        super().__init__()

    def gpu_side(self):
        pass


class CPUSide(object):

    def __init__(self) -> None:
        super().__init__()

    def cpu_side(self):
        pass


class WithSingularity(object):
    def singularity(self):
        pass


class CompileCodeGPUSing(CompileCode, GPUSide, WithSingularity):
    def __init__(self) -> None:
        super().__init__()

    def compile(self):
        super().compile()
