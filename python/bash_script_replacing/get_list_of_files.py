import os
import logging

'''
For the given path, get the List of all files in the directory tree 
'''


def get_list_of_files(dir_name):
    # create a list of file and sub directories
    # names in the given directory
    list_of_files = list()
    for (dirpath, dirnames, filenames) in os.walk(dir_name):
        list_of_files += [os.path.join(dirpath, file) for file in filenames]
    return list_of_files


def search_file(dir, filename):
    for (root, dirs, files) in os.walk(str(dir)):
        for file in files:
            if file == filename:
                # location_of_exe = root
                # path = os.path.join(root, file)
                tmp = os.path.join(root, file)
                logging.info("executable: {}".format(tmp))
                return tmp
