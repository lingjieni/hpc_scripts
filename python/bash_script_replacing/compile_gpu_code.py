import os, sys, errno
import logging
import subprocess
from pathlib import Path

from bash_script_replacing.check_MOAB_variables import *
from bash_script_replacing.check_if_gpu_available import *
from bash_script_replacing.loggin_level import ask_for_logging_level

sys.path.append(os.path.dirname(__file__))


def complie_gpu_code(project_dir, cluster, gpu_target):

    MOAB_JOBNAME = None
    MOAB_PROCOUNNT = None
    if cluster == 'local_machine':
        try:
            os.environ['CUDACXX']
        except Exception as e:
            os.environ['CUDACXX'] = '/usr/local/cuda-9.2/bin/nvcc'
            logging.info('set CUDACXX to {}'.format(os.environ['CUDACXX']))
        build_dir = project_dir / 'build_local'
        MOAB_JOBNAME = check_MOAB_variable(ENV_var='MOAB_JOBNAME', value_if_not_existed='INTERACTIVE')
        MOAB_PROCOUNNT = check_MOAB_variable(ENV_var='MOAB_PROCOUNNT', value_if_not_existed='2')
    else:
        build_dir = project_dir / 'build'
        MOAB_JOBNAME = check_MOAB_variable(ENV_var='MOAB_JOBNAME', value_if_not_existed='INTERACTIVE')
        MOAB_PROCOUNNT = check_MOAB_variable(ENV_var='MOAB_PROCOUNNT', value_if_not_existed='16')
    try:
        # print(project_dir/"build")
        os.makedirs(build_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    num_of_process_4_compiling = int(3 / 2 * int(MOAB_PROCOUNNT))
    try:
        cmake_args = []
        if cluster != 'local_machine':
            home_dir = Path(os.environ['HOME'])
            cmake_loc = home_dir / 'libs' / 'bin' / 'cmake'
        else:
            cmake_loc = 'cmake'
        cmake_args.append(cmake_loc)
        logging.info('Using cmake from {}'.format(cmake_loc))
        # cmake_args.append(ask_for_logging_level())
        cmake_args.append('..')
        cmake_args.append('-DCPU_ONLY=OFF')
        # if check_if_gpu_available():
        #     cmake_args.append('-DCPU_ONLY=OFF')
        # else:
        #     cmake_args.append('-DCPU_ONLY=ON')

        logging.info('Set logging level in c++ at '.format(cmake_args))
        # arg =
        os.chdir(build_dir)
        # print('where am I: ', os.getcwd())
        logging.info('Run {} in {}'.format(cmake_loc, os.getcwd()))
        p_cmake = subprocess.Popen(cmake_args)
        if p_cmake.wait() == 0:
            print()
        else:
            raise RuntimeError
        # make_args = []
        # print(make_args)
        make_args = ['make', gpu_target, '-j', str(num_of_process_4_compiling)]
        logging.info('Make target {} with {}'.format(gpu_target, num_of_process_4_compiling))
        p_make = subprocess.Popen(make_args)
        if p_make.wait() == 0:
            print()
        else:
            raise RuntimeError
    except Exception as e:
        logging.warning('Compiling did NOT finish correctly')
        raise
    return build_dir
