import os

from pathlib2 import Path

from cluster.cluster import ClusterName

import logging


class LocalPathSetting:

    def __init__(self, local_project_root_dir) -> None:
        self.local_project_root_dir = Path(local_project_root_dir)

    @property
    def local_build_dir(self) -> Path:
        return Path(self.local_project_root_dir) / 'build' / 'build_from_local'

    @property
    def cpp_code_dir(self) -> Path:
        return self.local_project_root_dir / 'cpp_code'

    @property
    def simulation_test_dir(self) -> Path:
        return self.cpp_code_dir / 'simulation_test'

    @property
    def simulation_test_heston(self) -> Path:
        return self.simulation_test_dir / 'heston'

    @property
    def simulation_test_heston_pf(self) -> Path:
        return self.simulation_test_heston / 'filtering' / 'pf'

    @property
    def simulation_test_heston_pmh(self) -> Path:
        return self.simulation_test_heston / 'parameter_estimation' / 'pmh'

    @property
    def simulation_test_heston_data_gen(self) -> Path:
        return self.simulation_test_heston / 'data_generating'

    @property
    def simulation_test_bates(self) -> Path:
        return self.simulation_test_dir / 'bates'

    @property
    def simulation_test_bates_pf(self) -> Path:
        return self.simulation_test_bates / 'filtering' / 'pf'

    @property
    def simulation_test_bates_pmh(self) -> Path:
        return self.simulation_test_bates / 'parameter_estimation' / 'pmh'

    @property
    def simulation_test_bates_data_gen(self) -> Path:
        return self.simulation_test_bates / 'data_generating'

    @property
    def simulation_test_svcj(self) -> Path:
        return self.simulation_test_dir / 'svcj'

    @property
    def simulation_test_svcj_pf(self) -> Path:
        return self.simulation_test_svcj / 'filtering' / 'pf'

    @property
    def simulation_test_svcj_data_gen(self) -> Path:
        return self.simulation_test_svcj / 'data_generating'


_local_root_dir = Path.home() / 'CLionProjects' / 'cuda_LN_PhD'
local_path = LocalPathSetting(local_project_root_dir=_local_root_dir)

defined_username = 'ka_hd7013'
_forcluster_local_root_dir = Path("/scratch") / defined_username / 'CLionProjects' / 'cuda_LN_PhD'
forcluster_local_path = LocalPathSetting(local_project_root_dir=_forcluster_local_root_dir)


class RemotePathSetting:
    """
    information about cluster/remote execution
    """

    def __init__(self, cluster_name: ClusterName, home_dir=Path('$PROJECT'), conda_env_name='cuda_LN_PhD',
                 python_version='3.7', working_dir=None) -> None:
        self.cluster_name = cluster_name
        self._conda_env_name = conda_env_name
        self._python_version = python_version
        self._home_dir = Path(home_dir)
        self._working_dir = Path(home_dir) if working_dir is None else Path(working_dir)

    @property
    def remote_project_dir(self):
        if self.cluster_name == ClusterName.hlr1:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD')
        elif self.cluster_name == ClusterName.forcluster:
            return str(self._working_dir / 'CLionProjects/cuda_LN_PhD')
        elif self.cluster_name == ClusterName.unicluster:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD')
        elif self.cluster_name == ClusterName.local:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD')
        else:
            raise ValueError('no cluster found')

    @property
    def remote_build_dir(self):
        if self.cluster_name == ClusterName.hlr1:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD/build/build_from_local')
        elif self.cluster_name == ClusterName.forcluster:
            return str(self._working_dir / 'CLionProjects/cuda_LN_PhD/build/build_from_local')
        elif self.cluster_name == ClusterName.unicluster:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD/build/build_from_local')
        elif self.cluster_name == ClusterName.local:
            return str(self._home_dir / 'CLionProjects/cuda_LN_PhD/build/build_from_local')
        else:
            raise ValueError('no cluster found')

    @property
    def remote_python_interpreter(self):
        if self.cluster_name == ClusterName.hlr1:
            return str(self._home_dir / 'anaconda3/envs' / self._conda_env_name / 'bin' / (
                    'python' + self._python_version))
        elif self.cluster_name == ClusterName.forcluster:
            return str(self._home_dir / 'anaconda3/envs' / self._conda_env_name / 'bin' / (
                    'python' + self._python_version))
        elif self.cluster_name == ClusterName.unicluster:
            return str(self._home_dir / 'anaconda3/envs' / self._conda_env_name / 'bin' / (
                    'python' + self._python_version))
        elif self.cluster_name == ClusterName.local:
            return str(self._home_dir / 'anaconda3/envs' / self._conda_env_name / 'bin' / (
                'python' + self._python_version))
        else:
            raise ValueError('no cluster found')

    def replace_with_tmp(self, original_path: Path):
        pass


hlr1_remote_path = RemotePathSetting(cluster_name=ClusterName.hlr1, home_dir='/home/fh1-project-ssmfin/hd7013')
forcluster_remote_path = RemotePathSetting(cluster_name=ClusterName.forcluster, home_dir='$HOME',
                                           working_dir='$HOME')
unicluster_remote_path = RemotePathSetting(cluster_name=ClusterName.unicluster, home_dir='/home/kit/fbv/hd7013')
local_remote_path = RemotePathSetting(cluster_name=ClusterName.local, home_dir=os.path.expandvars('$HOME'))


class ChooseCLuster:
    def __init__(self, cluster_name: ClusterName) -> None:
        self.cluster_name = cluster_name

    @property
    def remote_path(self) -> RemotePathSetting:
        try:
            if self.cluster_name == ClusterName.hlr1:
                return hlr1_remote_path
            elif self.cluster_name == ClusterName.forcluster:
                return forcluster_remote_path
            elif self.cluster_name == ClusterName.unicluster:
                return unicluster_remote_path
            else:
                raise ValueError('Cluster does not exist')
        except ValueError as e:
            logger = logging.getLogger(__name__)
            logger.error(e)
            raise e
