import sys
import os
from pathlib import Path


# from mpi4py import MPI




# define output location and log redirection
def add_python_path(python_src_folders, project_dir):
    output_dir = project_dir / 'output'
    log_dir = output_dir / 'log'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        os.makedirs(log_dir)
    elif not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # add PYTHONPATH
    python_src_folders.append(project_dir / 'src')

    for ele in python_src_folders:
        try:
            sys.path.index(str(ele))
        except ValueError:
            sys.path.append(str(ele))

# print(sys.path)
