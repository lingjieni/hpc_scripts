import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.ticker import FormatStrFormatter
from contextlib import redirect_stdout

def getMeasurementsFromTextfile(filepath):
    results = pd.DataFrame(columns=['threads','time'])
    regex = re.compile('(.*);(.*)')

    with open(filepath) as file:
        n_threads = 0
        time = 0

        for line in file:
            str = line.strip()
            match = regex.match(str)
            if not match is None:
                n_threads = int(match.groups()[0])
                time = float(match.groups()[1])
                results.loc[len(results)] = [n_threads, time]
        return results

def createBoxplotsAndStats(measurements):

    stats = measurements.groupby("threads").describe()
    with open("stats.txt", 'w') as f:
        with redirect_stdout(f):
            pd.set_option('display.max_rows', 500)
            pd.set_option('display.max_columns', 500)
            pd.set_option('display.width', 1000)
            print(stats)
            print("")
            print(stats.to_latex())

    measurements.boxplot('time', by='threads', notch=False, sym='+')
    #make outliers smaller
    mean_min_threads = measurements[measurements.threads ==
                                    measurements["threads"].min()]["time"].mean()
    count = len(measurements["threads"].unique())
    x = np.arange(1, count + 1)
    y = mean_min_threads / x
    plt.plot(x, y, '-r')

    plt.title("OpenMP parallelization")
    plt.suptitle("")
    plt.xlabel("threads")
    plt.ylabel("time [ms]")
    plt.grid(True)
    plt.tight_layout()
    plt.savefig("boxplot.png", dpi=300)
    plt.show()


if __name__ == '__main__':
    file = 'results_forcluster.txt'
    measurements = getMeasurementsFromTextfile(file)
    createBoxplotsAndStats(measurements)