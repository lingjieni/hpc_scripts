# Created by ln at 29.04.19

import yaml
import util.location_determination as sb
import bash_script_replacing.ssh_gpu_node as sgn
project_dir = sb.project_dir
cluster = sb.cluster
config = yaml.safe_load(open('./config/run_compare_gaussian_from_cpu'))
container = config['container']['name']
container_loc = config['container']['loc']
exec_loc = config['exec']['loc']
# exec = config['exec']['name'] + " 'random numbers from pure inverse method'"
exec = config['exec']['name']
sgn.exec_in_gpu_node(container_loc, container, exec_loc, exec)
