#!/usr/bin/env python3

from bash_script_replacing.compile_with_singularity import *

project_dir = sb.project_dir
project_name = sb.project_name
cluster = sb.cluster_obj
executable = 'MCS_SCVJ_1iter'
container = '/home/ln/CLionProjects/cuda_LN_PhD/phd_gpu_devel.sif'
_, target_dir = compile_with_singularity(project_dir, cluster, 'test-' + executable)
executable_loc = target_dir / executable
exec_in_singularity(container, [str(executable_loc)], nv=True)
# remote_path = '/home/ka/ka_fbv/ka_hd7013/cuda_LN_PhD_remote'
# upload_file2cluster(executable_loc, remote_path=remote_path)

# print(example_dir)

# run_exe(str(executable_loc))
