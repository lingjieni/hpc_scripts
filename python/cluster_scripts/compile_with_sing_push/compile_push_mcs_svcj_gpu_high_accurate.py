#!/usr/bin/env python3
import yaml
import util.location_determination as sb
import bash_script_replacing.compile_with_singularity as cws
import bash_script_replacing.push_file2cluster as pf2c

project_dir = sb.project_dir
project_name = sb.project_name
cluster = sb.cluster
config = yaml.safe_load(open('./config/mcs_svcj_high_accurate_gpu'))
executable = config['exec']
container = config['container']
remote_path = config['remote_path']
_, target_dir = cws.compile_with_singularity(project_dir, container, cluster, 'test-' + executable)
executable_loc = target_dir / executable
pf2c.upload_file2cluster(executable_loc, remote_path=remote_path)

