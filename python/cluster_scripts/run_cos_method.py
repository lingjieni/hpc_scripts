#!/usr/bin/env python3
import os, sys, errno
import subprocess

sys.path.append(os.path.dirname(__file__))

import path_tool as p

print(os.path.dirname(__file__))
project_name = "CUDAClionStarterProject"
project_dir, cluster = p.get_project_dir_and_location(project_name=project_name)

try:
    # print(project_dir/"build")
    os.makedirs(project_dir/'build')
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

print(cluster)
print(project_dir)
executable = 'cos_method'
p = subprocess.Popen([project_dir/'build'/executable])
rval = p.wait()
if rval == 0:
    print("exe successfully")
else:
    print(rval)

