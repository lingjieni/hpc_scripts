#!/usr/bin/env python3

import util.location_determination as sb
import settings.path_tool as pt
from bash_script_replacing.run_exe import *
from bash_script_replacing.compile_gpu_code import *

project_dir = sb.project_dir
cluster = sb.cluster
exmaples = 'spdlog_thrust'
build_dir = complie_gpu_code(project_dir, cluster, 'example-' + exmaples)
example_dir = pt.get_example_dir(build_dir=build_dir) / 'thrust_base'
# print(example_dir)

run_exe(str(example_dir / exmaples))
