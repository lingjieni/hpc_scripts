from pathlib2 import Path
from cluster.compiler import IntelCompiler
from cluster.mpi_command import IntelMPICommand
from util import yaml_helper
from util.extract_targets_execs import extract_execs


def exec_mpi_on_hlr1(build_folder, project_folder, exec_root_folder, cluster, job_id, target_list, **kwargs):
    # local part
    num_of_nodes = cluster.get_num_of_nodes(job_id)
    # double the number of nodes due to socket allocation
    np = kwargs.get('np', num_of_nodes * 2)
    profiling = kwargs.get('profiling', False)
    clean = kwargs.get('profiling', False)

    hostfile_str = cluster.jobs_list.loc[job_id, 'NODE LIST']
    hostfile_str = ','.join(hostfile_str)

    yaml_file = str(Path(exec_root_folder) / 'targets_and_execs_list.yaml')
    targets_and_execs_list = yaml_helper.load_yaml_file(yaml_file)

    # remote part
    compiler = IntelCompiler(build_folder, project_folder, cluster)
    compiler.run_compilation(target_list, job_id=job_id, profiling=profiling, clean=clean, intel_env=True)
    # compiler.run_compilation(target_list, intel_env=True, clean=True, job_id=job_id)
    execs_list, relative_bin_path = extract_execs(target_list, targets_and_execs_list)

    mpi_command_intel = IntelMPICommand(np=np, hostfile=hostfile_str)
    # cluster.run_mpi(mpi_executable=executable, mpi_command=mpi_command_intel,
    #                job_id=job_id, omp_thread_num=8, allocate_natively=True, debug_output_mpi=True)
    for exec_name in execs_list:
        executable = str(Path(build_folder) / relative_bin_path / exec_name)
        cluster.run_mpi(mpi_executable=executable, mpi_command=mpi_command_intel,
                        job_id=job_id, omp_thread_num=10, mpi_pin_domain="socket", omp_places="cores",
                        omp_proc_bind="spread", debug_output_mpi=True)


def exec_mpi_on_forcluster(build_folder, project_folder, exec_root_folder, cluster, job_id, target_list, **kwargs):
    # local part
    np = kwargs.get('np', cluster.get_num_of_nodes(job_id))
    profiling = kwargs.get('profiling', False)
    clean = kwargs.get('profiling', False)

    hostfile_str = cluster.jobs_list.loc[job_id, 'NODE LIST']
    hostfile_str = ','.join(hostfile_str)

    yaml_file = str(Path(exec_root_folder) / 'targets_and_execs_list.yaml')
    targets_and_execs_list = yaml_helper.load_yaml_file(yaml_file)

    # remote part
    compiler = IntelCompiler(build_folder, project_folder, cluster)
    compiler.run_compilation(target_list, job_id=job_id, profiling=profiling, clean=clean, intel_env=True)
    # compiler.run_compilation(target_list, intel_env=True, clean=True, job_id=job_id)
    execs_list, relative_bin_path = extract_execs(target_list, targets_and_execs_list)

    mpi_command_intel = IntelMPICommand(np=np, hostfile=hostfile_str)
    # cluster.run_mpi(mpi_executable=executable, mpi_command=mpi_command_intel,
    #                job_id=job_id, omp_thread_num=8, allocate_natively=True, debug_output_mpi=True)
    for exec_name in execs_list:
        executable = str(Path(build_folder) / relative_bin_path / exec_name)
        cluster.run_mpi(mpi_executable=executable, mpi_command=mpi_command_intel,
                        job_id=job_id, omp_thread_num=16, mpi_pin_domain="socket", omp_places="cores",
                        omp_proc_bind="spread", debug_output_mpi=True)


def exec_benchmark_on_hlr1(build_folder, project_folder, exec_root_folder, cluster, job_id, target_list, **kwargs):
    profiling = kwargs.get('profiling', False)
    clean = kwargs.get('profiling', False)

    hostfile_str = cluster.jobs_list.loc[job_id, 'NODE LIST']
    hostfile_str = ','.join(hostfile_str)

    yaml_file = str(Path(exec_root_folder) / 'targets_and_execs_list.yaml')
    targets_and_execs_list = yaml_helper.load_yaml_file(yaml_file)

    # remote part
    compiler = IntelCompiler(build_folder, project_folder, cluster)
    compiler.run_compilation(target_list, job_id=job_id, profiling=profiling, clean=clean, intel_env=True)
    execs_list, relative_bin_path = extract_execs(target_list, targets_and_execs_list)

    for exec_name in execs_list:
        executable = str(Path(build_folder) / relative_bin_path / exec_name)
        command = ['bash -c', executable]
        cluster.run(,,,,,


def exec_benchmark_on_forcluster(build_folder, project_folder, exec_root_folder, cluster, job_id, target_list,
                                 **kwargs):
    profiling = kwargs.get('profiling', False)
    clean = kwargs.get('profiling', False)

    hostfile_str = cluster.jobs_list.loc[job_id, 'NODE LIST']
    hostfile_str = ','.join(hostfile_str)

    yaml_file = str(Path(exec_root_folder) / 'targets_and_execs_list.yaml')
    targets_and_execs_list = yaml_helper.load_yaml_file(yaml_file)

    # remote part
    compiler = IntelCompiler(build_folder, project_folder, cluster)
    compiler.run_compilation(target_list, job_id=job_id, profiling=profiling, clean=clean, intel_env=True)
    execs_list, relative_bin_path = extract_execs(target_list, targets_and_execs_list)

    for exec_name in execs_list:
        executable = str(Path(build_folder) / relative_bin_path / exec_name)
        command = ['bash -c', executable]
        cluster.run(,,,,,
