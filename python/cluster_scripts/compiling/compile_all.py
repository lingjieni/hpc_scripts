#!/usr/bin/env python3

import util.location_determination as sb
from bash_script_replacing.compile_gpu_code import *

project_dir = sb.project_dir
cluster = sb.cluster
complie_gpu_code(project_dir=project_dir, cluster=cluster, gpu_target='all')
