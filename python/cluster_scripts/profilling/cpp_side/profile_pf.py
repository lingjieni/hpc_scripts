import util.location_determination as sb
from bash_script_replacing.compile_cpu_code import *
from subprocess import Popen

project_dir = sb.project_dir
cluster = sb.cluster
cpu_exe = 'main_pf'
build_dir = compile_cpu_code(project_dir, cluster, 'profiling-' + cpu_exe, 'release')

args = ['valgrind', '--tool=callgrind', str(build_dir / cpu_exe)]
p = Popen(args)
