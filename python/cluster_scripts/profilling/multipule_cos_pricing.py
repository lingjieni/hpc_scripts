#!/usr/bin/env python3

from settings.path_tool import *
from bash_script_replacing.compile_gpu_code import *

gpu_exe = 'cos_pricing'
build_dir = complie_gpu_code(project_dir, cluster, 'example-' + gpu_exe)
example_dir = get_example_dir(build_dir=build_dir) / 'thrust_pricing'

All = [1, 2, 3, 4]
args = ['time', str(example_dir / gpu_exe)]

processes = []
for No in All:
    # f = os.tmpfile()
    p = subprocess.Popen(args)
    processes.append(p)

for p in processes:
    p.wait()
