#!/usr/bin/env python3

import util.location_determination as bs
from bash_script_replacing.profile_gpu_code import *
from bash_script_replacing.compile_gpu_code import *

project_name = bs.project_name
gpu_exes = ['polymorphism']

project_dir, cluster = get_project_dir(project_name=project_name)
path2exe = project_dir / 'build' / 'tests'
# compile all the executables
for gpu_exe in gpu_exes:
    complie_gpu_code(project_dir=project_dir, cluster=cluster, gpu_target='test-' + gpu_exe)
    profile_output = gpu_exe + '.nvvp'
    profile_gpu_code(path2exe=path2exe, gpu_exe=gpu_exe, profile_output=profile_output,
                     cluster=cluster)