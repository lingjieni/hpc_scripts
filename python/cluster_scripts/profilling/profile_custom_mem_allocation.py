#!/usr/bin/env python3

import os, sys

sys.path.append(os.path.dirname(__file__))

import util.location_determination as sb

import settings.path_tool as pt
from bash_script_replacing.profile_gpu_code import *

project_name = sb.project_name
gpu_exe = 'custom_temporary_allocation'
profile_output = gpu_exe + '.nvvp'

project_dir, cluster = pt.get_project_dir(project_name=project_name)
profile_gpu_code(project_dir=project_dir, gpu_exe=gpu_exe, profile_output=profile_output,
                 cluster=cluster)
