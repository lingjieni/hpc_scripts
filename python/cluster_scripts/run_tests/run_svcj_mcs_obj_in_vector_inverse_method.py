#!/usr/bin/env python3

import util.location_determination as sb
from bash_script_replacing.run_exe import *
from bash_script_replacing.compile_gpu_code import *

project_dir = sb.project_dir
cluster = sb.cluster
test = 'svcj_mcs_obj_in_vector_inverse_method'
build_dir = complie_gpu_code(project_dir, cluster, 'test-' + test)
example_dir = build_dir / 'tests' / 'model_dynamic'
# print(example_dir)

run_exe(str(example_dir / test))
