from bash_script_replacing.compile_cpu_code import compile_cpu_code

import util.location_determination as sb

project_dir = sb.project_dir
cluster = sb.cluster
test = 'pf_seq_simulated_data'
build_dir = compile_cpu_code(project_dir, cluster, 'test-' + test, 'release')
example_dir = build_dir/ 'bin'




