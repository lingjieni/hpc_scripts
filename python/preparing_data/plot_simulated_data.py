from pathlib2 import Path
import yaml
import io
import numpy as np
import matplotlib.pyplot as plt

path2cpp_code = Path('../../cpp_code')
simulated_data_file = path2cpp_code / 'tests' / 'pf' / 'input' / 'simulated_data' / 'simulated_data.yaml'

with io.open(simulated_data_file, 'r') as stream:
    data_loaded = yaml.safe_load(stream)

log_return_index = np.array(data_loaded['log_return_index'])
latent_states = np.array(data_loaded['latent_states'])
Vs = latent_states[:, 0]
plt.plot(Vs)
plt.show()
# plt.plot(log_return_index)
# plt.show()
