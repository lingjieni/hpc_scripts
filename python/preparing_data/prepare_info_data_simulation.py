import numpy as np
import logging


def generate_homo_surfaces(num_of_pts: int, maturities, moneyness):
    single_surface = {}
    list_of_surfaces = []
    for m in maturities:
        single_surface[m] = list(moneyness)
    for i in range(num_of_pts):
        list_of_surfaces.append(single_surface)
    logger = logging.getLogger(__name__)
    logger.info("generate homo surfaces across time")
    return list_of_surfaces


def generate_nan_option_prices(list_of_surfaces):
    single_option_prices = {}
    nan_option_prices = []
    for surface in list_of_surfaces:
        for m, strikes in surface.items():
            single_option_prices[m] = ['.nan'] * len(strikes)
        nan_option_prices.append(single_option_prices)
    return nan_option_prices


if __name__ == "__main__":
    list_of_surfaces_with_strike = generate_homo_surfaces(num_of_pts=252, maturities=[0.3, 0.5, 1.],
                                                          moneyness=[90., 100., 110.])
    atm_impl_vol = 0.02
    m_in_unit = np.array([-4, -2, 0., 0.5, 1])
    maturities = np.array([10., 45., 120., 252.]) / 252.
