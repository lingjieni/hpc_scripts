import pickle
from pathlib2 import Path
import io
import yaml
import numpy as np

from preparing_data.prepare_info_data_simulation import generate_homo_surfaces

path2file = Path('.') / 'data_from_MK'
print(str(path2file))
#
with open(path2file / 'result.pickle', 'rb') as handle:
    result = pickle.load(handle)

with open(path2file / 'simulated_data.pickle', 'rb') as handle:
    simulated_data = pickle.load(handle)

days_in_year = 252
delta_t = 1. / days_in_year
interest_rate = 0.0319
dividend_rate = 0.
num_of_pts = 252

true_V = simulated_data['true_V']
surface = simulated_data['surface']
maturities = surface[0]
strikes = surface[1]

output_data = {}
log_level = simulated_data['log_return']
log_return = log_level[1:] - log_level[0:-1]
output_data['log_return_index'] = ['.nan'] + list(map(float, list(log_return)))
output_data['log_underlying_prices'] = list(map(float, list(log_level)))
output_data['option_prices'] = []
for price_list in simulated_data['option_data']:
    for prices, maturity in zip(price_list, maturities):
        output_data['option_prices'].append({maturity: (list(map(float, list(prices))))})

list_of_surfaces = generate_homo_surfaces(num_of_pts=252, maturities=maturities, moneyness=strikes)
output_data['list_of_surfaces'] = list_of_surfaces

path2cpp_code = Path('../../cpp_code')
out4estimation_loc = path2cpp_code / 'tests' / 'pf' / 'input' / 'simulated_data' / 'data_from_MK_for_estimation.yaml'
yaml.SafeDumper.ignore_aliases = lambda *args: True
drifts_term_structure = []
for i in range(num_of_pts):
    drifts_term_structure.append(list(map(float, list(np.repeat(interest_rate - dividend_rate, len(maturities))))))
names_of_obs = ['log_underlying_prices', 'option_prices']
with io.open(out4estimation_loc, 'w') as stream:
    yaml.safe_dump({'diff_maturities_across_time_span': maturities}, stream, default_flow_style=False,
                   allow_unicode=True)
    yaml.safe_dump({'delta_ts': list(map(float, np.repeat(delta_t, num_of_pts)))}, stream, default_flow_style=False,
                   allow_unicode=True)
    yaml.safe_dump({'interest_rates': list(map(float, np.repeat(interest_rate, num_of_pts)))}, stream,
                   default_flow_style=False, allow_unicode=True)
    yaml.safe_dump({'dividend_yields': list(map(float, np.repeat(dividend_rate, num_of_pts)))}, stream,
                   default_flow_style=False, allow_unicode=True)
    yaml.safe_dump({'drifts_term_structure': drifts_term_structure}, stream,
                   default_flow_style=False, allow_unicode=True)
    yaml.safe_dump(output_data, stream)

out4simulation_loc = path2cpp_code / 'tests' / 'pf' / 'input' / 'simulated_data' / 'simulated_data_MK.yaml'
list_simulated_data_latent = simulated_data['latent_states'].tolist()
# for list in list_simulated_data_latent:
#     list = list(map(float, list))
with io.open(out4simulation_loc, 'w') as stream:
    yaml.safe_dump({'latent_states': list_simulated_data_latent}, stream)
    # yaml.safe_dump({'': simulated_data['ture_V']}, stream)
