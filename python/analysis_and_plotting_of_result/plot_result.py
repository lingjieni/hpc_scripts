from pathlib2 import Path
import yaml
import io
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.ticker as mtick

def plot_line(data_pts, label = None, color = None):
    plt.plot(data_pts, label=label, linestyle='-', linewidth=1.0, ms=4, color = color)

def plot_dash_line(data_pts, label = None, color = None):
    plt.plot(data_pts, label=label, linestyle='--', marker='o', linewidth=1.0, ms=4, color = color)


def plot_filtering_result_for_svcj(result_loaded, simulated_data_loaded):
    # result_loaded = result_file

    # filtered_latent_states = np.array(result_loaded['filtered_states_only_return'])
    # filtered_Vs_with_return = np.array(result_loaded['filtered_Vs_only_return'])[1:]
    # filtered_Vs_with_option = np.array(result_loaded['filtered_Vs_with_option'])[1:]
    filtered_Vs_with_return = np.array(result_loaded['filtered_Vs_only_return'])
    filtered_Vs_with_option = np.array(result_loaded['filtered_Vs_with_option'])
    simulated_data = np.array(simulated_data_loaded['latent_states'])
    simulated_Vs = simulated_data[:, 0][:-2]
    # simulated_Vs = simulated_data[:, 0][:-1]

    mpl.style.use('seaborn')
    plt.subplot(211)
    plt.plot(filtered_Vs_with_option, label='filtered Vs with option', linestyle='--', marker='o', linewidth=1.0, ms=4)
    plt.plot(filtered_Vs_with_return, label='filtered Vs with only return', linestyle='-', linewidth=1.0, ms=4)
    plt.plot(simulated_Vs, label='simulated Vs', linestyle='--', linewidth=1.0, marker='o', ms=4, color='black')
    plt.legend()
    ax = plt.subplot(212)
    ax.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1.))
    abs_diff_with_option = np.abs(simulated_Vs - filtered_Vs_with_option) / simulated_Vs
    abs_diff_with_return = np.abs(simulated_Vs - filtered_Vs_with_return) / simulated_Vs
    plt.plot(abs_diff_with_option, linewidth=1.0)
    print("mean abs error with OPTION in percentage", np.mean(abs_diff_with_option))
    # plt.plot(abs_diff_with_return, linewidth=1.0)
    print("mean abs error with RETURN ONLY in percentage", np.mean(abs_diff_with_return))
    plt.show()
    return 0


if __name__ == "__main__":
    nth_path = 0
    path2cpp_code = Path('../../cpp_code')
    result_file_name = 'result_' + str(nth_path) + '.yaml'
    simulated_data_file_name = 'simulated_data_' + str(nth_path) + '.yaml'
    result_file = path2cpp_code / 'tests' / 'pf' / 'output' / 'simulated_data' / result_file_name
    simulated_data_file = path2cpp_code / 'tests' / 'pf' / 'input' / 'simulated_data' / simulated_data_file_name
    with io.open(result_file, 'r') as stream:
        result_loaded = yaml.safe_load(stream)
    with io.open(simulated_data_file, 'r') as stream:
        simulated_data_loaded = yaml.safe_load(stream)
    plot_filtering_result_for_svcj(result_loaded, simulated_data_loaded)
