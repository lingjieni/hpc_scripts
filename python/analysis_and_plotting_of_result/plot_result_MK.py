from pathlib2 import Path
from analysis_and_plotting_of_result.plot_result import plot_filtering_result_for_svcj

path2cpp_code = Path('../../cpp_code')
result_file = path2cpp_code / 'tests' / 'pf' / 'output' / 'simulated_data' / 'result_MK.yaml'
simulated_data_file = path2cpp_code / 'tests' / 'pf' / 'input' / 'simulated_data' / 'simulated_data_MK.yaml'
plot_filtering_result_for_svcj(result_file, simulated_data_file)

