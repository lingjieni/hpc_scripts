import yaml
import io
import numpy as np


def load_data_with_yaml(path2file):
    with io.open(path2file, 'r') as stream:
        data_loaded = yaml.safe_load(stream)
    return data_loaded
