from datetime import datetime

def try_parsing_time(text):
    for fmt in ('%H:%M:%S', '%d-%H:%M:%S'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid date format found')