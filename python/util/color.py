import matplotlib.pyplot as plt

from python_pkg.estimation.util.plotting import SnSColorPlatte

if __name__ == '__main__':
    plt.plot([1, 2, 3, 4], color=SnSColorPlatte().red)
    plt.plot([1, 1.5, 3, 5] * 2, color='tab:red')
    plt.ylabel('some numbers')
    plt.show()
