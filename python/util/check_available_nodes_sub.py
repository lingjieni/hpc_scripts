#!/usr/bin/env python3.7
import subprocess

showbf = 'showbf'
list_of_resources = {'standard': 'standard','old gpu': 'gpu', 'new gpu': 'gpu-sky', 'old best': 'best', 'new best': 'best-sky'}
for key, value in list_of_resources.items():
    print('==============')
    print(key.upper() + ':')
    subprocess.run([showbf, '-f', value])
