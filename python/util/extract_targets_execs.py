from util.path_helper import get_current_cmake_root_path
from util.yaml_helper import load_yaml_file


def extract_execs(target_list, targets_execs_list):
    execs_list = []
    for target in target_list:
        for target_exec in targets_execs_list['build_target']:
            if target_exec['target'] == target:
                execs_list.append(target_exec['executable'])
                break

    return execs_list, targets_execs_list['bin_path']


def extract_execs_for_python_scripts(python_root_path, target_list):
    current_cmake_path = get_current_cmake_root_path(python_root_path)
    targets_and_execs_list = load_yaml_file(current_cmake_path / 'targets_and_execs_list.yaml')
    execs_list, _ = extract_execs(target_list, targets_and_execs_list)
    return execs_list
