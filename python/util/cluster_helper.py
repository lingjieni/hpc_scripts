import pickle
import logging
import invoke
import os
from pathlib2 import Path
from patchwork import transfers
from invoke import Context
from fabric import Connection
from util.path_helper import PathHelper
from util.yaml_helper import load_yaml_file
from util.yaml_helper import write_yaml_file
from util.target_exec_helper import TargetExecHelper
from cluster import compiler, mpi_command
from cluster.cluster import Cluster, LocalMachine, ClusterName, OnForClusterLogin
from settings import defined_username


class ClusterHelper:
    def __init__(self, path_helper: PathHelper, cluster_name: ClusterName, job_id=None) -> None:
        self.path_helper = path_helper
        self.cluster_name = cluster_name
        self._current_job_id = job_id

    @property
    def job_id(self):
        """
        job id on cluster, which are loaded from local job_id.yaml file as default job id, but user can define it via job_id

        :return:
        """
        if self._current_job_id is None:
            # if it is on a local machine
            if self.cluster_name is ClusterName.local:
                return None
            job_id_yaml = load_yaml_file(self.path_helper.python_script_root_path / 'job_id.yaml')
            return job_id_yaml['job_id_' + self.cluster_name.name]
        else:
            return self._current_job_id

    @property
    def cluster_pickle_loc(self):
        loc = str(
            self.path_helper.python_script_root_path / 'cluster_pickle' / (self.cluster_name.name + '.pickle'))
        return loc

    @property
    def cluster_obj(self):
        if self.cluster_name is ClusterName.local:
            return LocalMachine()
        else:
            with open(self.cluster_pickle_loc, 'rb') as file:
                cluster = pickle.load(file)
            # cluster.c_login.open()
            return cluster

    @property
    def build_dir(self):
        # if self.cluster_obj.cluster_name == 'mlswiso':
        #     cpu_type = self.cluster_obj.jobs_list.loc[self.job_id, 'NODE TYPE']
        #     if cpu_type == 'best-sky':
        #         _build_dir = self.path_helper.exec_location_build_dir / 'skylake'
        #     else:
        #         _build_dir = self.path_helper.exec_location_build_dir / cpu_type
        # else:
        _build_dir = self.path_helper.exec_location_build_dir
        return _build_dir

    def update_job_id_yaml(self, job_id):
        job_id_yaml = load_yaml_file(self.path_helper.python_script_root_path / 'job_id.yaml')
        job_id_yaml['job_id_' + self.cluster_name.hlr1.name] = str(job_id)
        write_yaml_file(self.path_helper.python_script_root_path / 'job_id.yaml', job_id_yaml)

    def reset_path_helper(self, new_path_helper=None):
        if new_path_helper is not None:
            self.path_helper = new_path_helper

    def _upload_files(self, local, remote):
        return self.cluster_obj.c_login.put(str(local), str(remote))

    def _download_folder(self, local: Path, remote: Path):
        with self.cluster_obj.c_login as c:
            sftp = c.sftp()
            # since $HOME does not fit into check of folder
            abs_remote_folder = Path(c.run(" ".join(["echo", str(remote)])).stdout.rstrip())
            files_in_folder = sftp.listdir(str(abs_remote_folder))
            for file in files_in_folder:
                sftp.get(remotepath=str(abs_remote_folder / file), localpath=str(local / file))

    def _extract_rel_loc(self, local_loc: Path):
        return local_loc.relative_to(self.path_helper.local_project_dir)

    def exec_mapping_loc(self, local_loc: Path):
        """

        :param local_loc:
        :return: mapping local file to execution location
        """
        return self.path_helper.exec_location_project_dir / self._extract_rel_loc(local_loc)

    def sync_file(self, local_file_loc):
        dst_file = self.exec_mapping_loc(local_file_loc)
        dst = dst_file.parent
        if isinstance(self.cluster_obj.c_login, Context):
            logging.info('no sync needed')
        elif isinstance(self.cluster_obj.c_login, Connection):
            with self.cluster_obj.c_login as c:
                try:
                    dst = c.run('echo ' + str(dst)).stdout.rstrip()
                    c.put(local=str(local_file_loc), remote=str(dst))
                except invoke.exceptions.UnexpectedExit:
                    c.run(' '.join(['mkdir', '-p', str(dst)]))
                    logging.info('create folder recursively {}'.format(dst))
                    c.put(local=str(local_file_loc), remote=str(dst))
        logging.info('sync to {}'.format(dst))


class LocalExecutor:
    def __init__(self, path_helper: PathHelper) -> None:
        self.path_helper = path_helper
        self.target_exec_helper = TargetExecHelper(path_helper)
        self.local_machine_obj = LocalMachine()

    def exec(self, target_list, path_helper=None):
        if path_helper is not None:
            self.path_helper = path_helper
            self.target_exec_helper = TargetExecHelper(path_helper)

        relative_bin_path = self.target_exec_helper.relative_bin_path
        exec_list = self.target_exec_helper.get_execs_from_target_exec_yaml(target_list=target_list)
        logger = logging.getLogger(__name__)
        for exec_name in exec_list:
            executable = str(Path(self.path_helper.exec_location_build_dir) / relative_bin_path / exec_name)
            logger.info("execute: {}".format(executable))
            self.local_machine_obj.run(command=[executable])


class ClusterHelperOnLogin(ClusterHelper):
    def __init__(self, user, path_helper: PathHelper, cluster_name: ClusterName, job_id=None) -> None:
        super().__init__(path_helper, cluster_name, job_id)
        self.user = user

    @property
    def cluster_obj(self) -> Cluster:
        if self.cluster_name == ClusterName.forcluster:
            cluster = OnForClusterLogin(user=self.user)
        else:
            raise ValueError('no corresponding user')
        return cluster


class CompilationHelper:
    """
    define src, build path and so on
    """

    def run_cmake(self, **kwargs):
        pass

    def run_make(self, target_list, **kwargs):
        pass

    # def rsync_compiled_files(self, dst_loc):
    #     pass


class LocalCompilationHelper(CompilationHelper):
    def __init__(self, path_helper: PathHelper, build_dir=None, copy_dst_dir=None, compiler_env=None) -> None:
        self.compiler_env = compiler_env
        self.path_helper = path_helper
        if build_dir is not None:
            self.build_dir = str(build_dir)
        else:
            self.build_dir = self.path_helper.exec_location_build_dir
        if copy_dst_dir is not None:
            self.copy_dst_dir = str(copy_dst_dir)
        self.project_dir = self.path_helper.exec_location_project_dir
        self.cluster_obj = LocalMachine()
        self.compiler_obj = compiler.IntelCompiler(self.build_dir,
                                                   self.project_dir,
                                                   self.cluster_obj)
        self.copy_dst_dir = None

    def run_cmake(self, **kwargs):
        profiling = kwargs.pop('profiling', False)
        clean = kwargs.pop('clean', False)

        if self.compiler_env is not None:
            self.compiler_obj.run_cmake(profiling=profiling, clean=clean, intel_env=True,
                                        compiler_env=self.compiler_env)
        else:
            self.compiler_obj.run_cmake(profiling=profiling, clean=clean, intel_env=True)

    def run_make(self, target_list, **kwargs):

        if self.compiler_env is not None:
            self.compiler_obj.run_make(build_target=target_list, compiler_env=self.compiler_env, **kwargs)
        else:
            self.compiler_obj.run_make(build_target=target_list, **kwargs)
        if self.copy_dst_dir is not None:
            tmp_build_bin_with_back_slash = str(Path(self.build_dir) / "bin/")
            dst_bin_abs_dir = Path(os.path.expandvars(str(Path(self.copy_dst_dir) / "bin")))
            # only sync bin
            logger = logging.getLogger(__name__)
            if not Path.exists(dst_bin_abs_dir):
                logger.info("create bin folder in dst {}".format(str(dst_bin_abs_dir)))
                Path.mkdir(dst_bin_abs_dir, exist_ok=True, parents=True)
            self.cluster_obj.rsync(local=tmp_build_bin_with_back_slash, dst=self.copy_dst_dir)
            logger.info(
                "sync only bin folder from {} to {}".format(tmp_build_bin_with_back_slash, str(self.copy_dst_dir)))
            # self.cluster_obj.rsync(local=self.build_dir, dst=self.copy_dst_dir)


class RemoteCompilationHelper(ClusterHelper, CompilationHelper):
    def __init__(self, path_helper: PathHelper, cluster_name: ClusterName, job_id=None) -> None:
        super().__init__(path_helper, cluster_name, job_id)

    @property
    def source_intellibs(self):
        if self.cluster_name == ClusterName.local:
            return ['']
        else:
            return ['source $HOME/intellibs.sh']

    def run_cmake(self, **kwargs):
        profiling = kwargs.pop('profiling', False)
        clean = kwargs.pop('clean', False)
        compiler_obj = compiler.IntelCompiler(self.build_dir,
                                              self.path_helper.exec_location_project_dir,
                                              self.cluster_obj)

        compiler_obj.run_cmake(job_id=self.job_id, profiling=profiling, clean=clean, intel_env=True,
                               compiler_env=self.source_intellibs)

    def run_make(self, target_list, **kwargs):
        compiler_obj = compiler.IntelCompiler(self.build_dir,
                                              self.path_helper.exec_location_project_dir,
                                              self.cluster_obj)
        compiler_obj.run_make(build_target=target_list, job_id=self.job_id, compiler_env=self.source_intellibs,
                              **kwargs)


class RemoteCompilationOnLoginInTMPHelper(RemoteCompilationHelper):
    """
    adjust on cmake with login, build in tmp, then copy that to self.build_dir
    """

    def __init__(self, path_helper: PathHelper, cluster_name: ClusterName, job_id=None, rsync_dst=None,
                 on_login=False) -> None:
        super().__init__(path_helper, cluster_name, job_id)
        self.on_login = on_login
        self.rsync_dst = rsync_dst
        index = Path(self.build_dir).parts.index('$HOME')
        self.tmp_build_dir = Path('$TMP').joinpath(*Path(self.build_dir).parts[index + 1:])
        if self.on_login and self.cluster_name == ClusterName.forcluster:
            self.cluster_obj_onlogin = OnForClusterLogin(user=defined_username)

    @property
    def cluster_obj(self) -> Cluster:
        if self.on_login and self.cluster_name == ClusterName.forcluster:
            return self.cluster_obj_onlogin
        else:
            return super().cluster_obj

    def run_cmake(self, **kwargs):
        profiling = kwargs.pop('profiling', False)
        clean = kwargs.pop('clean', False)
        c = self.cluster_obj
        arch = c.get_job_node_arch(job_id=self.job_id)
        compiler_obj = compiler.IntelCompilerOnLogin(self.tmp_build_dir,
                                                     self.path_helper.exec_location_project_dir,
                                                     c)

        compiler_obj.run_cmake(job_id=self.job_id, profiling=profiling, clean=clean, intel_env=True,
                               compiler_env=self.source_intellibs, arch=arch)

    def run_make(self, target_list, **kwargs):
        compiler_obj = compiler.IntelCompilerOnLogin(self.tmp_build_dir,
                                                     self.path_helper.exec_location_project_dir,
                                                     self.cluster_obj)
        compiler_obj.run_make(build_target=target_list, job_id=self.job_id, compiler_env=self.source_intellibs,
                              **kwargs)
        # sync from tmp to home
        # sync contents in folder
        tmp_build_bin_with_back_slash = str(self.tmp_build_dir / "bin/")
        build_bin_abs_dir = Path(os.path.expandvars(str(self.build_dir / "bin")))
        # only sync bin
        logger = logging.getLogger(__name__)
        if not Path.exists(build_bin_abs_dir):
            logger.info("create bin folder in dst {}".format(str(build_bin_abs_dir)))
            Path.mkdir(build_bin_abs_dir, exist_ok=True, parents=True)
        self.cluster_obj.rsync(local=tmp_build_bin_with_back_slash, dst=self.build_dir)
        logger.info("sync only bin folder")


class CLusterRunPythonScriptHelper(ClusterHelper):

    def __init__(self, path_helper: PathHelper, python_interpreter, cluster_name=ClusterName.hlr1,
                 python_path=None) -> None:
        super().__init__(path_helper, cluster_name)
        self.python_interpreter = str(python_interpreter)
        self.python_path = python_path

    @property
    def pythonpath_at_exec_location(self):
        tmp = []
        for path in self.python_path:
            tmp.append(str(Path(self.path_helper.exec_location_project_dir) / path))
        return tmp

    def run_python_script_on_cluster(self, relative_script_file_loc):
        """

        :param relative_script_file_loc: relative path to project dir
        :return:
        """
        abs_script_file_loc = self.path_helper.exec_location_project_dir / relative_script_file_loc
        env = {'PYTHONPATH': ":".join(self.pythonpath_at_exec_location)}
        command = [self.python_interpreter, str(abs_script_file_loc)]
        self.cluster_obj.run(command=command, job_id=self.job_id, env=env)


class ClusterExecHelper(ClusterHelper):
    def __init__(self, path_helper: PathHelper, cluster_name: ClusterName, job_id=None) -> None:
        super().__init__(path_helper, cluster_name, job_id)
        self.target_exec_helper = TargetExecHelper(self.path_helper)

    def reset_path_helper(self, new_path_helper=None):
        super().reset_path_helper(new_path_helper)
        self.target_exec_helper.path_helper = new_path_helper

    def get_result(self, target_list):
        if self.cluster_name is ClusterName.local:
            logging.info('executed locally no need for copying result')
        else:
            exec_list = self.path_helper.get_execs_from_target_exec_yaml(target_list)
            for exec_name in exec_list:
                # make sure output folder exists locally
                output_dir = self.path_helper.local_output_path / exec_name
                if not output_dir.exists():
                    output_dir.mkdir(parents=True, exist_ok=True)
                    logging.info('create folder {}'.format(self.path_helper.local_output_path))

                self._download_folder(remote=self.path_helper.exec_loc_output_path / exec_name,
                                      local=self.path_helper.local_output_path / exec_name)

                logging.info('get result from {} to {}'.format(self.path_helper.exec_loc_output_path / exec_name,
                                                               self.path_helper.local_output_path / exec_name))

    def upload_config(self, target_list):
        # upload/refresh config files created locally
        config_list = self.target_exec_helper.get_config_list_for_target(target_list=target_list)
        logger = logging.getLogger(__name__)
        for config in config_list:
            relative_loc = (self.path_helper.local_config_path / config).relative_to(self.path_helper.local_project_dir)
            try:
                upload_result = self._upload_files(self.path_helper.local_project_dir / relative_loc,
                                                   self.cluster_obj.working_dir / relative_loc)
            except FileNotFoundError as e:
                logger.error(
                    "Config file does not exists in {}".format(str(self.cluster_obj.working_dir / relative_loc)))
                # self.cluster_obj.c_login.run()
                raise e
            logger.info('successfully upload from {} to {}'.format(upload_result.local, upload_result.remote))

    def exec(self, target_list, path_helper=None):
        if path_helper is not None:
            self.reset_path_helper(path_helper)
        relative_bin_path = self.target_exec_helper.relative_bin_path
        exec_list = self.target_exec_helper.get_execs_from_target_exec_yaml(target_list=target_list)
        c = self.cluster_obj
        # remote part
        if isinstance(c, Connection):
            self.upload_config(target_list=target_list)
        if self.cluster_name is not ClusterName.local:
            load_libs = 'source ~/intellibs.sh &&'
        else:
            load_libs = ''
        for exec_name in exec_list:
            executable = str(Path(self.path_helper.exec_location_build_dir) / relative_bin_path / exec_name)
            c.run(command=[load_libs, executable], job_id=self.job_id)

    def exec_with_mpi(self, target_list, path_helper=None, **kwargs):
        """

        :param path_helper:
        :param target_list:
        :param kwargs:
        :return:
        """
        c = self.cluster_obj
        if path_helper is not None:
            self.reset_path_helper(path_helper)

        # local part
        try:
            num_of_nodes = c.get_num_of_nodes(self.job_id)
        except KeyError as e:
            logger = logging.getLogger(__name__)
            logger.error('no job with id {}'.format(self.job_id))
            raise e
        num_of_cores_per_node = c.get_num_of_cores_per_node(self.job_id)
        # double processes via socket, reduce threads into half
        omp_thread_num = int(num_of_cores_per_node / 2)
        # double the number of nodes due to socket allocation
        np = kwargs.pop('np', num_of_nodes * 2)

        hostfile_str = c.jobs_list.loc[self.job_id, 'NODE LIST']
        hostfile_str = ','.join(hostfile_str)

        # remote part
        if isinstance(c, Connection):
            self.upload_config(target_list=target_list)
        execs_list = self.target_exec_helper.get_execs_from_target_exec_yaml(target_list=target_list)
        relative_bin_path = self.target_exec_helper.relative_bin_path

        mpi_command_intel = mpi_command.IntelMPICommand(np=np, hostfile=hostfile_str)
        for exec_name in execs_list:
            executable = str(Path(self.build_dir) / relative_bin_path / exec_name)
            c.run_mpi(mpi_executable=executable, mpi_command=mpi_command_intel,
                      job_id=self.job_id, omp_thread_num=omp_thread_num, mpi_pin_domain="socket",
                      omp_places="cores",
                      omp_proc_bind="spread", debug_output_mpi=True)


class ClusterExecHelperOnLogin(ClusterExecHelper):
    def __init__(self, path_helper: PathHelper, cluster_name: ClusterName, job_id=None) -> None:
        super().__init__(path_helper, cluster_name, job_id)

    @property
    def cluster_obj(self) -> Cluster:
        if self.cluster_name == ClusterName.forcluster:
            return OnForClusterLogin(user=defined_username)
        else:
            return super().cluster_obj

    def get_result(self, target_list):
        logging.info('Execution on login node, no need to download result')
