from pathlib2 import Path
import logging


def save_fig(fig, fig_loc: Path):
    fig_dir = fig_loc.parent
    if not fig_dir.exists():
        fig_dir.mkdir()
        logger = logging.getLogger(__name__)
        logger.warning("create folder {} for keeping figs".format(fig_dir))
    fig.savefig(fig_loc)
