import os
import logging
from typing import List
from pathlib2 import Path
from util.yaml_helper import load_yaml_file
from util.pickle_helper import load_pickle_file
from settings import local_path
from cluster.cluster import ClusterName


def relative_path_to_absolute_path(file, relative_path):
    return os.path.join(os.path.dirname(file), relative_path)


class PathHelper:
    def __init__(self, python_working_dir,
                 local_project_dir=local_path.local_project_root_dir,
                 exec_location: ClusterName = ClusterName.local,
                 exec_location_project_dir=None,
                 exec_location_build_dir=None) -> None:
        """
        collection of some useful paths, all the cpp path dependency decided in CMakeList

        :param python_working_dir: it tells where the local_cmake_root_path.yaml is and let code find 'cmake list root'
        :param local_project_dir:
        :param exec_location: where cpp/python code is executed
        :param exec_location_project_dir:
        :param exec_location_build_dir:
        """

        self.python_script_root_path = Path(python_working_dir)
        self.local_project_dir = Path(local_project_dir)
        self.current_cmake_root = Path(load_yaml_file(self.python_script_root_path / 'local_cmake_root_path.yaml')[
                                           'local_cmake_root_path'])
        self.exec_location = exec_location
        self.exec_location_project_dir = Path(
            exec_location_project_dir) if exec_location_project_dir is not None else None
        self.exec_location_build_dir = Path(exec_location_build_dir) if exec_location_build_dir is not None else None

    def relative_path(self):
        pass

    def get_current_cmake_root_path(self, python_scripts_root_path: Path):
        """

        :param python_scripts_root_path:
        :return: current_cmake_root output_path input_path config_path
        """
        self.python_script_root_path = python_scripts_root_path
        self.current_cmake_root = Path(load_yaml_file(python_scripts_root_path / 'local_cmake_root_path.yaml')[
                                           'local_cmake_root_path'])
        return self.current_cmake_root

    @property
    def exec_loc_cmake_root(self):
        return self.exec_location_project_dir / (self.current_cmake_root.relative_to(self.local_project_dir))

    @property
    def exec_location_str(self):
        return self.exec_location.name

    @property
    def local_output_path(self):
        return self.current_cmake_root / 'output' / self.exec_location_str

    @property
    def exec_loc_output_path(self):
        return self.exec_loc_cmake_root / 'output' / self.exec_location_str

    @property
    def local_input_path(self):
        return self.current_cmake_root / 'input'

    @property
    def exec_loc_input_path(self):
        return self.exec_loc_cmake_root / 'input'

    @property
    def local_config_path(self):
        return self.current_cmake_root / 'config'

    @property
    def target_exec_yaml(self):
        """
        search in local source folder, the same location as python_script_root_path
        :return:
        """
        return load_yaml_file(self.python_script_root_path.parent / 'targets_and_execs_list.yaml')

    @property
    def relative_bin_path(self):
        return self.target_exec_yaml['bin_path']

    def get_execs_from_target_exec_yaml(self, target_list: List[str] = None):
        """
        get executable name from target

        :param target_list:
        :return:
        """
        exec_list = []
        if target_list is None:
            for target_exec in self.target_exec_yaml['build_target']:
                exec_list.append(target_exec['executable'])
        else:
            for target in target_list:
                for target_exec in self.target_exec_yaml['build_target']:
                    if target == target_exec['target']:
                        exec_list.append(target_exec['executable'])

        if not exec_list:
            logger = logging.getLogger(__name__)
            logger.error(
                "No targets: {} found in target_list: {}".format(target_list, self.target_exec_yaml['build_target']))
            raise KeyError(
                "No targets: {} found in target_list: {}".format(target_list, self.target_exec_yaml['build_target']))
        return exec_list

    def get_config_yaml_for_target(self, target_list: List[str] = None):
        """

        :param target_list:
        :return:
        """
        exec_list = self.get_execs_from_target_exec_yaml(target_list)
        config_yaml_list = []
        for executable in exec_list:
            config_yaml_list.append(executable + '.yaml')
        return config_yaml_list

    def get_config_node_for_target(self, target_list: List[str] = None):
        """
        get config node in local config path

        :param target_list:
        :return:
        """
        exec_list = self.get_execs_from_target_exec_yaml(target_list)
        config_node_list = []
        for executable in exec_list:
            config_node_list.append(load_pickle_file(self.local_config_path / (executable + '.pickle')))
        return config_node_list

    def get_output_file_in_config(self, target_list=None, **kwargs):
        """
        get output file location, which defined in config

        :param target_list:
        :return:
        """
        output_file_list = []
        nth_path = kwargs.pop('nth_path', None)
        exec_list = self.get_execs_from_target_exec_yaml(target_list)
        config_list = self.get_config_node_for_target(target_list=target_list)
        for executable, config in zip(exec_list, config_list):
            if nth_path is None:
                output_file_list.append(self.local_output_path / executable / config.output_file_name)
            else:
                tmp_file_name = '_'.join([config.output_file_name, str(nth_path)]) + '.yaml'
                output_file_list.append(
                    self.local_output_path / executable / tmp_file_name)
        return output_file_list


class PathHelperLocalOnly(PathHelper):
    def __init__(self, python_working_dir, local_project_dir=local_path.local_project_root_dir,
                 exec_location_project_dir=None,
                 exec_location_build_dir=None) -> None:
        super().__init__(python_working_dir, local_project_dir, exec_location_project_dir, exec_location_build_dir)
        self.exec_location_project_dir = self.local_project_dir
        self.exec_location_build_dir = self.local_project_dir / 'build'
