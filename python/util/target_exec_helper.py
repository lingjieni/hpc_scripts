from util.path_helper import PathHelper
from util.yaml_helper import load_yaml_file


class TargetExecHelper:
    def __init__(self, path_helper: PathHelper = None) -> None:
        self.path_helper = path_helper

    @property
    def target_exec_yaml(self):
        """
        search in local source folder, the same location as python_script_root_path
        :return:
        """
        return load_yaml_file(self.path_helper.python_script_root_path.parent / 'targets_and_execs_list.yaml')

    @property
    def relative_bin_path(self):
        return self.target_exec_yaml['bin_path']

    def get_execs_from_target_exec_yaml(self, target_list=None):
        """

        :param target_list:
        :return:
        """
        exec_list = []
        if target_list is None:
            for target_exec in self.target_exec_yaml['build_target']:
                exec_list.append(target_exec['executable'])
        else:
            for target in target_list:
                for target_exec in self.target_exec_yaml['build_target']:
                    if target == target_exec['target']:
                        exec_list.append(target_exec['executable'])
        if not exec_list:
            raise ValueError('target is not in list from {}'.format(self.path_helper.current_cmake_root))
        return exec_list

    def get_config_list_for_target(self, target_list=None):
        """

        :param target_list:
        :return:
        """
        exec_list = self.get_execs_from_target_exec_yaml(target_list)
        config_yaml_list = []
        for executable in exec_list:
            config_yaml_list.append(executable + '.yaml')
        return config_yaml_list
