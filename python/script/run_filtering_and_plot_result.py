# complie filtering code
from bash_script_replacing.compile_cpu_code import compile_cpu_code
from bash_script_replacing.run_exe import run_exe


if __name__ == "__main__":
    # project_dir = sb.project_dir
    # cluster = sb.cluster
    executable_name = 'pf_seq_simulated_Index'
    build_dir = compile_cpu_code('example-' + executable_name, 'release')
    executable = build_dir / 'bin' / executable_name
    run_exe(executable)
