import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.ticker import FormatStrFormatter
from contextlib import redirect_stdout


def getMeasurementsFromLogfile(log):
    results = dict()
    particleRegex = re.compile('num_of_particles: (\d+)')
    rankRegex = re.compile('\[\d+\]\s*MPI startup\(\):\s*(\d+)\s*\d+\s*.+\s*{.*}')
    timeMeasurementRegex = re.compile('.+PF with MPI TOOK (.+) ms')

    n_particles = 0
    n_processes = 0

    for line in log.splitlines():
        line_str = line.strip()

        isParticleCount = particleRegex.match(line_str)
        if not isParticleCount is None:
            n_particles = int(isParticleCount.groups()[0])
            if not n_particles in results:
                results[n_particles] = pd.DataFrame(columns=['worker_processes', 'time'])
            continue

        isProcessCount = rankRegex.match(line_str)
        if not isProcessCount is None:
            n_processes = int(isProcessCount.groups()[0]) + 1
            n_processes -= 1  # one process is managing process
            continue

        isTimeMeasurement = timeMeasurementRegex.match(line_str)
        if not isTimeMeasurement is None:
            time = float(isTimeMeasurement.groups()[0])
            results[n_particles].loc[len(results[n_particles])] = [n_processes, time]
            continue

    return results


def createBoxplotsAndStats(measurements):

    for n_particles, values in sorted(measurements.items()):
        values_to_print = values.copy()
        values_to_print['time'] = values_to_print['time'].div(1000).round(5)

        stats = values_to_print.groupby("worker_processes").describe()
        with open("../scaling_results/stats_" + str(n_particles) + "particles.txt", 'w') as f:
            with redirect_stdout(f):
                pd.set_option('display.max_rows', 500)
                pd.set_option('display.max_columns', 500)
                pd.set_option('display.width', 1000)
                print(str(n_particles) + " particles")
                print(stats)
                print("")
                print(stats.to_latex())

        ax_boxplot = values_to_print.boxplot('time', by='worker_processes',
                                             notch=False, sym='+')

        mean_min_processes = values_to_print[values_to_print.worker_processes ==
                                      values_to_print["worker_processes"].min()]["time"].mean()
        min_processes = values_to_print["worker_processes"].min()
        max_processes = values_to_print["worker_processes"].max()
        count = len(values_to_print["worker_processes"].unique())
        x = np.arange(1, count + 1)
        y = mean_min_processes / (2 ** (x-1))
        plt.plot(x, y, '-r')

        plt.title(str(n_particles) + " particles")
        plt.suptitle("")
        plt.xlabel("mpi worker processes")
        plt.ylabel("time [s]")
        plt.grid(True)
        plt.yscale('log', basey=2)
        plt.gca().yaxis.set_major_formatter(mticker.ScalarFormatter())
        plt.gca().yaxis.get_major_formatter().set_scientific(False)
        plt.gca().yaxis.get_major_formatter().set_useOffset(False)
        plt.tight_layout()
        #plt.gcf().set_size_inches(8.27, 11.69)
        plt.savefig("../scaling_results/boxplot_" + str(n_particles) + "particles.png", dpi=300)
        plt.show()


